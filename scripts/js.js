var openBoxRequest = function(cUrl, el, size, rID) {
	if (cUrl == '3637') {
		var requestID = '&requestID=' + rID;
	} else {
		var requestID = '';
	}
	if (size && size[0] && size[1]) {
		var sizeBox = [
			size[0],
			size[1]
		];
	} else {
		var sizeBox = [
			680,
			540
		];
	}
	var cleanUrl = (cUrl.charAt(0) == '/') ? cUrl.substr(1) : cUrl;
	var url = basepath + cleanUrl + '?openBoxRequest=1' + requestID;
	var width = sizeBox[0];
	var height = sizeBox[1];
	var box = Ext.MessageBox.show({
		title : '',
		msg : '<iframe src="' + url + '" style="border:0px #FFFFFF none;" name="kontaktBoxIframe" scrolling="no" frameborder="1" marginheight="0px" marginwidth="0px" height="' + (height - 40) + 'px" width="100%"></iframe>',
		animEl : el,
		width : width,
		height : height,
		modal : true,
		fn : function() {
			Ext.MessageBox.hide();
		}
	});
	var dlg = box.getDialog();
	dlg.resizeTo(width, height);
};
function setBoxRequest(className) {
	var cElems = Ext.select('.' + className);
	if (cElems.getCount() > 0) {
		cElems.each(function(el) {
			el.on('click', function() {
				openBoxRequest(el.dom.href, this, null);
			}, undefined, {
				preventDefault : true
			});
		});
	}
};
function SliderInit(Slider) {
	var slide = {
		Type : Slider.Type || 'slide', // slide, fade
		Container : Ext.get(Slider.Container || 'mySlider_container'),
		Control_l : Ext.get(Slider.Control_l || 'mySlider_control_l'),
		Control_r : Ext.get(Slider.Control_r || 'mySlider_control_r'),
		Content : Ext.get(Slider.Content || 'mySlider_content'),
		ToSlide : Ext.get(Slider.ToSlide || 'mySlider_toslide'),
		Elements : Ext.select('.' + (Slider.Element || 'mySlider_element')),
		Anim : Slider.Anim || false
	};
	if (isNull(slide.Container) || isNull(slide.Control_l) || isNull(slide.Control_r) || isNull(slide.Content) || isNull(slide.ToSlide) || isNull(slide.Elements.elements[0])) {
		return;
	}
	(slide.Anim === true) ? slide.Anim = {
		easing : 'easeOut',
		duration : .5,
		delay : 0
	} : slide.Anim;
	slide.Content.setPositioning({
		position : 'relative'
	});
	if (slide.Type == 'fade') {
		slide.ToSlide.setPositioning({
			left : '0px',
			top : '0px',
			position : 'relative'
		});
		slide.ToSlide.setWidth(Ext.get(slide.Elements.elements[0]).getWidth());
		slide.ToSlide.setHeight(Ext.get(slide.Elements.elements[0]).getHeight());
		slide.Elements.setPositioning({
			left : '0px',
			top : '0px',
			position : 'absolute'
		});
		slide.Elements.setStyle({
			opacity : 0,
			zIndex : 5
		});
		Ext.get(slide.Elements.elements[0]).setStyle({
			opacity : 1,
			zIndex : 10
		});
	} else if (slide.Type == 'slide') {
		slide.ToSlide.setPositioning({
			left : '0px',
			top : '0px',
			position : 'absolute'
		});
		slide.Elements.Width = Ext.get(slide.Elements.elements[0]).getWidth();
		slide.Elements.Margins = Ext.get(slide.Elements.elements[0]).getMargins();
		slide.Elements.MaxWidth = slide.Elements.Width + slide.Elements.Margins.right;
		slide.Content.MaxWidth = slide.Content.getWidth();
	}
	slide.Counter = 0;
	slide.Control_l.on('click', slideTo, slide.Control_l, slide);
	slide.Control_r.on('click', slideTo, slide.Control_r, slide);
}
function slideTo(ev, scope, Parameter) {
	Parameter.Direction = !!(this.dom.id == Parameter.Control_l.dom.id || this.dom.id == Parameter.Control_r.dom.id);
	if (Parameter.Direction == false) {
		return;
	}
	if (Parameter.Type == 'slide') {
		if (this.dom.id == Parameter.Control_l.dom.id && Parameter.Counter > 0) {
			Parameter.ToSlide.moveTo(Parameter.ToSlide.getX() + slide.Elements.MaxWidth, Parameter.ToSlide.getY(), Parameter.Anim);
			Parameter.Counter--;
		} else if (this.dom.id == Parameter.Control_r.dom.id && Parameter.Counter < (Parameter.Content.MaxWidth / Parameter.Elements.MaxWidth)) {
			Parameter.ToSlide.moveTo(Parameter.ToSlide.getX() - slide.Elements.MaxWidth, Parameter.ToSlide.getY(), Parameter.Anim);
			Parameter.Counter++;
		}
	} else if (Parameter.Type == 'fade') {
		Parameter.Elements.setStyle({
			zIndex : 5
		});// Nach rechts: counter zIndex gr��er als folgender, danach tauschen
		if (this.dom.id == Parameter.Control_l.dom.id && Parameter.Counter > 0) {
			if (Parameter.Anim !== false && !Parameter.Anim.callback) {
				Parameter.Anim.callback = function(el) {
					el.setStyle({
						zIndex : 5
					});
					Ext.get(Parameter.Elements.elements[Parameter.Counter]).setStyle({
						zIndex : 10
					})
				};
			}
			Ext.get(Parameter.Elements.elements[Parameter.Counter]).setStyle({
				zIndex : 10
			}).setOpacity(0, Parameter.Anim);
			Parameter.Counter--;
		} else if (this.dom.id == Parameter.Control_r.dom.id && Parameter.Counter < Parameter.Elements.elements.length - 1) {
			if (Parameter.Anim !== false && !Parameter.Anim.callback) {
				Parameter.Anim.callback = function(el) {
					el.setStyle({
						zIndex : 5
					});
					Ext.get(Parameter.Elements.elements[Parameter.Counter]).setStyle({
						zIndex : 10
					})
				};
			}
			Ext.get(Parameter.Elements.elements[Parameter.Counter]).setStyle({
				zIndex : 10
			}).setOpacity(0, Parameter.Anim);
			Parameter.Counter++;
		}
		Ext.get(Parameter.Elements.elements[Parameter.Counter]).setOpacity(1);
	}
}
// Detailbild tauschen
var zoomConfig = {
	zoomWindowOffetx : 72,
	zoomWindowOffety : -6,
	zoomWindowWidth : 500,
	zoomWindowHeight : 498,
    scrollZoom      : false,
	loadingIcon : '/_/scripts/ext/resources/images/default/grid/loading.gif'
};
function SwapDetailPic(Handler) {
	var container = Ext.select('#ans_shop_media_detail img').first();
    Handler.href = (Handler.dataset && Handler.dataset.href) ? Handler.dataset.href : Handler.href;
    var src = Handler.href.replace(/url\(["']?/, '').replace(/["']?\)/, '').replace(/\/media\/med_/, '/medium/med_');
    var ez = $(container.dom).data('elevateZoom');
	container.dom.src = Handler.href;
	ez.swaptheimage(container.dom.src, src);
}
/** ************************************** */
var extendLogin = function(lnk) {
	var box = Ext.get('kopflogin_data');
	if (box) {
		var mode = box.isDisplayed() == false || box.isDisplayed() == 'none' ? 'block' : 'none';
		box.setDisplayed(mode);
		checkLoginBox(lnk);
	}
};
/** * Loginbox ein/ ausblenden / steuern ** */
var checkLoginBox = function(lnk) {
	var box = Ext.get('kopflogin_data');
	var lnktoappend = lnk || 'lh_login';
	var appendbox = Ext.get(lnktoappend);
	if (box) {
		if (box.isDisplayed() == true) {
			var posx = appendbox.getX() - (box.getWidth() / 2 + 20);
			var posy = appendbox.getY() + appendbox.getHeight();
			var username = box.select('input[name=username]');
			if (username && username.elements && username.elements[0]) {
				Ext.get(username.elements[0]).dom.focus();
			}
			box.setX(posx);
			box.setY(posy);
		}
	}
};
function uniqueButton() {
	Ext.select('#site_article .cs3button').each(function(el) {
		el.replaceClass('cs3button', 'mgbutton');
		el.setStyle({
			fontSize : '12px',
			width : 'auto'
		});
	});
}
var registerExtWindows = function(classname, lybcall) {
	var links = Ext.select('a[class^=' + classname + '] ', true);
	var lyb = (lybcall && lybcall != false && lybacll != '') ? lybcall : '674e8089ce17730071c984baf55689da';
	if (links) {
		links.each(function(el) {
            var lnk = el.dom,
                w = 616,
                wt = '576';
            if( lnk.getAttribute( 'href' ).match( /\?size\=auto/ ) ) {
                w = 820,
                wt = '800';
            }
            Ext.get( lnk ).on( 'click', function( ev, el ) {
				var box = Ext.MessageBox.show({
					title : '',
					msg : '<img src=\"' + basepath + '_/scripts/ext/resources/images/default/grid/loading.gif\" alt=\"\" style=\"border:0; vertical-align:middle;\"> Loading...',
					animEl : el,
                    width: w,
					height : 610,
					modal : true,
					fn : function() {
						Ext.MessageBox.hide();
					}
				});
				Ext.Msg.getDialog().body.addClass('printdiv');
				var lnkopt = lnk.href.split('?');
				var url = '';
                if( lnkopt[1] && lnkopt[1] != '' && !lnkopt[1].match( /\?size\=auto/ ) ) {
					url = lnk.href + '&_lyb=' + lyb;
				} else {
					url = lnk.href + '?_lyb=' + lyb;
				}
				Ext.Ajax.request({
					url : url,
					success : function(req) {
						var txt = decode(req.responseText);
						var dlg = box.getDialog();
                        box.updateText( '<div style=\"width:' + wt + 'px; height:560px; overflow:auto;\">' + txt.html + '</div>' );
                        dlg.resizeTo( w, 610 );
						var scrollToA = Ext.get('lieferkosten');
						if (scrollToA) {
							Ext.get(Ext.select('.ext-mb-text>div').elements[0]).scrollTo('top', scrollToA.dom.offsetTop - 20);
						}
					}
				});
            }, this, {
                preventDefault: true
            } );
		});
	}
};
var registerMailWindow = function(conf) {
	var def = {
		linkID : conf.linkID || false,
		linkClass : conf.linkClass || false,
		contentID : conf.contentID || 'content_mailmanagerform',
		wWidth : conf.wWidth || 680,
		wHeight : conf.wHeight || 580,
		wID : conf.wID || 'mailWindow',
		autoShow : conf.autoShow || false,
		successFunc : conf.successFunc || false,
		errorFunc : conf.errorFunc || false
	};
	if ((Ext.get(def.linkID) != null || Ext.select('.' + def.linkClass).elements.length > 0) && Ext.get(def.contentID) != null) {
		var cContent = Ext.get(def.contentID);
		var dBoxObj = {
			tag : 'div',
			id : 'mailWindowContainer'
		};
		var dialogID = Ext.DomHelper.append(document.body, dBoxObj);
		var msgConf = {
			title : '',
			animateTarget : cLink,
			width : def.wWidth,
			height : def.wHeight,
			modal : true,
			draggable : false,
			resizable : false,
			closable : true,
			collapsible : false,
			shadow : true,
			fn : function() {
				Ext.MessageBox.hide();
			}
		};
		function genMsgBox(boxID, boxConf, Scope) {
			if (dBoxObj.id == boxID.id + boxID.className) {
				boxConf.animateTarget = Scope;
				var dlg = new Ext.BasicDialog(boxID, boxConf);
				dlg.header.remove();
				dlg.body.createChild({
					tag : 'div',
					id : def.wID
				});
				dlg.addKeyListener(27, dlg.hide, dlg);
				cContent.appendTo(def.wID);
				dlg.show();
				dlg.resizeTo(def.wWidth, def.wHeight);
			} else {
				var dlg = Ext.DialogManager.get(dBoxObj.id);
				dlg.show();
			};
			if (def.successFunc != false) {
				def.successFunc(true);
			};
		};
		if (Ext.get(def.linkID) != null && Ext.select('.' + def.linkClass).elements.length == 0) {
			var cLink = Ext.get(def.linkID);
			msgConf.animateTarget = cLink;
			cLink.on('click', function() {
				genMsgBox(dialogID, msgConf, cLink);
			}, undefined, {
				preventDefault : true
			});
		} else if (Ext.get(def.linkID) == null && Ext.select('.' + def.linkClass).elements.length > 0) {
			var cLinks = Ext.select('.' + def.linkClass);
			msgConf.animateTarget = false;
			cLinks.each(function(el) {
				el.on('click', function() {
					genMsgBox(dialogID, msgConf, el);
				}, undefined, {
					preventDefault : true
				});
			});
		};
		if (def.autoShow === true) {
			genMsgBox(dialogID, msgConf);
		};
	} else if ((Ext.get(def.linkID) != null || Ext.select('.' + def.linkClass).elements.length > 0) && Ext.get(def.contentID) == null && Ext.get(def.errorFunc) != false) {
		def.errorFunc(true);
	} else {
		return;
	};
};
/* Kannst Objekte Sichtbarkeit ein und ausschalten */
function handleDisplay(dObj) {
	var setDisplay = (Ext.get(dObj).isDisplayed() === true) ? 'none' : 'block';
	Ext.get(dObj).setDisplayed(setDisplay);
}
/* Facebook Share Button */
function fbShare() {
	window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(location.href), 'facebook-share-dialog', 'width=626,height=436');
}
var spListener = {
    spLockDelay : 2000,
    spLocked    : false,
    spTimerID   : 'spTimer',
    delay       : function(fnc, args) {
        if(true === spListener.spLocked) {
            window.clearTimeout(window[spListener.spTimerID]);
        }
        window[spListener.spTimerID]    = window.setTimeout(function() {
            if('function' === typeof fnc) {
                fnc(args);
            }
            spListener.spLocked = false;
        }, spListener.spLockDelay);
        spListener.spLocked = true;
    }
};
recon.on('spOnLoad', function(st) {
    if(st.reader.jsonData.totalFound == 0) {
        spListener.delay(function(st) {
            var searchedTerm = {
                mod : 'search',
                data : st.baseParams.query
            };
            searchedTerm = {
                _data : '[' + base64_encode(Ext.encode(searchedTerm)) + ']'
            };
            searchedTerm = Ext.urlEncode(searchedTerm);
            document.location.href = '/4687?' + searchedTerm;
        }, st);
        st.reader.jsonData.html.evalScripts();
    }
});
recon.on('spNoResults', function(st) {
	Ext.get('spresult').setDisplayed('none');
});
function spOnNoResult() {
	var data = document.location.href.substr(document.location.href.search('_data'));
	if (data) {
		data = data.replace('_data=', '');
		data = Ext.urlDecode('term=' + data).term;
		if (data.search(/\[/) != -1 && data.search(/\]/) != -1 && data.search(/\[/) < data.search(/\]/)) {
			data = data.replace(/[\[\]]/g, '');
		} else {
			return;
		};
		data = base64_decode(data);
		data = Ext.decode(data);
		if (data.mod && data.mod == 'search' && data.data && data.data.length > 0) {
			var spNoResultField = Ext.get('sp_searchedterm_noresult');
			if (spNoResultField) {
				spNoResultField.update(data.data);
			};
		};
	};
};

var buyingbutton = null, buyingID = null;
var onConfigureOptions = function(obj, param, opts) {
	if (buyingbutton!=null) {
		$(".ans_shop_interact_bts .bskt_"+buyingID).parent().html(buyingbutton);
		buyingbutton = null;
	}
    var replaceData = ['price', 'baseprice'];
    for(var int = 0; int < replaceData.length; int++) {
        if(obj.data.hasOwnProperty(replaceData[int])) {
                if(obj.data[replaceData[int]].search(/\./) != -1 && parseInt(obj.data[replaceData[int]].split(/\./)[1]) == 0) {
                    obj.data[replaceData[int]]  = parseInt(obj.data[replaceData[int]].split(/\./)[0]);
                }
        }
    }
	if (('available' in obj.data)) {
		var el = Ext.get('bskt_' + param._id);
		var rml = Ext.get('reminderlink_' + param._id);
		if (obj.data.available == 0) {
			buyingID = $(obj['attrset']).attr("id").slice(-4);
			rml && rml.show();
			el && el.setDisplayed('none');
			el && el.up('div.attributlink') && el.up('div.attributlink').hide(true);
			spinners && spinners[param._mod + '_' + param._id] && spinners[param._mod + '_' + param._id].splitter.hide();
			recon.on('showReminderinline', prepareReminderForm, window, [
				obj,
				param,
				opts
			]);
			showReminderinline('content', param._id, '', '1');
		} else {
			rml && rml.setDisplayed('none');
			el && el.show();
			el && el.up('div.attributlink').show();
			spinners && spinners[param._mod + '_' + param._id] && spinners[param._mod + '_' + param._id].splitter.show() && spinners[param._mod + '_' + param._id].setValue(1);
			recon.un('showReminderinline', prepareReminderForm);
		}
	} else {
		Ext.get('reminderlink').hide(true);
	}
};

var showReminderinline = function(a, b, c, f) {
  Ext.QuickTips.disable();
  var e = "",
    d = 0,
    g = "",
    h = 0;
  isObject(a) ? (d = a.getAttribute("modid"), g = a.getAttribute("moddate"), e = a.getAttribute("mod")) : (d = b, e = a, g = c, h = f || 0);
  a = trfilename + "?_func=shRmd&_stRmMod=" + e + "&_stRmID=" + d + "&_stRmDate=" + g + "&_stRmStatus=" + h;
  "undefined" != typeof configureOptions && (b = configureOptions(!0, "attrmatrix_" + String(d), null, "reminder"), isArray(b) && (a += "&" + b.join("&") + "&_unique=1&_id=" + d));
  Ext.Ajax.request({
    url: a,
    success: function(a) {
      var b = function(a) {
        if("ok" == a) {
          a = e.getValues();
          var b = Ext.get("remindType"),
            b = b ? b.getValue() : "mail",
            d = !1,
            c;
          b.match(/mail/) && "customermail" in a && (Ext.Msg.getDialog().show(), c = e.items.get("customermail"), isEmpty(a.customermail) || !a.customermail.match(/[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}\b/) ? (c.el.addClass("invalid"), c.markInvalid("E-Mail-Adresse fehlerhaft"), d = !0) : c.el.removeClass("invalid"));
          b.match(/sms/) && "customermobile" in a && (Ext.Msg.getDialog().show(), c = e.items.get("customermobile"), isEmpty(a.customermobile) || !a.customermobile.match(/\+?[\d\s\.\-/]*/) ? (c.el.addClass("invalid"), c.markInvalid("Telefon-Nummer fehlerhaft"), d = !0) : c.el.removeClass("invalid"));
          d || setReminder()
        }
      };    
      if (buyingbutton ==null) {
	      buyingbutton = $(".ans_shop_interact_bts #bskt_"+buyingID).parent().html();
	      $(".ans_shop_interact_bts #bskt_"+buyingID).parent().html($(a.responseText).addClass("bskt_"+buyingID));
      } 
      msgBox(translate("Erinnerung"), a.responseText, "okcancel", b);
      $("#x-msg-box").hide();
      var e = new Ext.form.BasicForm("reminder");
      a = Ext.getDom("reminder");
      for(var d = 0; d < a.elements.length; d += 1) {
        var c = a.elements[d],
          f = new Ext.form.Field({
            name: c.name,
            id: c.name
          });
        f.applyTo(c);
        e.add(f)
      }
      recon.fireEvent("showReminderinline", [e]);
      new Ext.KeyMap(e.el, [{
        key: Ext.EventObject.ENTER,
        fn: function(a, e) {
          e.stopEvent();
          b("ok")
        },
        scope: e
      }])
    }
  })
}, setReminder = function() {
  var a = new Ext.form.BasicForm("reminder"),
    b = a.getValues(),
    c = trfilename + "?_func=stRmd";
  if("undefined" != typeof configureOptions) {
    var f = configureOptions(!0, "attrmatrix_" + String(b.rmd_itemid));
    isArray(f) && f.length && (c += "&" + f.join("&") + "&_unique=1&_id=" + b.rmd_itemid)
  }
  msgBox(translate("Erinnerung"), '<img src="' + basepath + '_/pics/loading.gif" style="vertical-align:middle; margin-right: 4px;">' + translate("Daten werden &uuml;bermittelt...."), "nobuttons");
  Ext.Ajax.request({
    url: c,
    method: "post",
    params: b,
    success: function(b) {
      b = decode(b.responseText);
      var c;
      isObject(b) ? (c = isFunction(window[b.callback]) ? window[b.callback] : Ext.emptyFn, b.error ? b.error && msgBox(translate("Fehler"), translate(b.text || "Es ist ein Fehler aufgetreten"), "ok") : msgBox(translate("Erinnerung"), translate(b.text || "Erinnerung wurde hinzugef&uuml;gt."), "ok", c)) : msgBox(translate("Fehler"), translate("Es ist ein Fehler aufgetreten"), "ok");
      new Ext.KeyMap(a.el, [{
        key: Ext.EventObject.ENTER,
        fn: function(a, b) {
          b.stopEvent();
          c("ok")
        },
        scope: a
      }])
    }
  });
  Ext.QuickTips.enable()
}, confirmSMS = function(a) {
  "ok" === a && (a = Ext.Ajax.serializeForm("reminder"), Ext.Ajax.request({
    method: "post",
    url: "?_func=stSMSCode",
    success: function(a) {
      a = decode(a.responseText);
      isObject(a) ? a.error ? a.error && msgBox(translate("Fehler"), translate(a.text || "Es ist ein Fehler aufgetreten"), "ok") : msgBox(translate("Erinnerung"), translate(a.text || "Erinnerung wurde hinzugef&uuml;gt."), "ok") : msgBox(translate("Fehler"), translate("Es ist ein Fehler aufgetreten"), "ok")
    },
    params: a
  }))
}, updateReminder = function(a) {
  Ext.Ajax.request({
    url: document.location.pathname + "?_func=shRmdMl&_del=1&_delId=" + a,
    success: function() {
      updatePanel("shRmdMl", "")
    }
  })
}, setRemindType = function(a) {
  switch(a.value) {
  case "mail":
    Ext.get("sms").setDisplayed(!1);
    Ext.getDom("mail").style.display = Ext.isIE && (Ext.isIE8 || Ext.isIE9 || Ext.isIE7 || Ext.isIE6) ? "block" : "table-row";
    break;
  case "sms":
    Ext.get("mail").setDisplayed(!1);
    Ext.getDom("sms").style.display = Ext.isIE && (Ext.isIE8 || Ext.isIE9 || Ext.isIE7 || Ext.isIE6) ? "block" : "table-row";
    break;
  case "mailsms":
    Ext.getDom("mail").style.display = Ext.isIE && (Ext.isIE8 || Ext.isIE9 || Ext.isIE7 || Ext.isIE6) ? "block" : "table-row", Ext.getDom("sms").style.display = Ext.isIE && (Ext.isIE8 || Ext.isIE9 || Ext.isIE7 || Ext.isIE6) ? "block" : "table-row"
  }
  a = Ext.Msg.getDialog();
  var b = Ext.util.TextMetrics.measure(a.body, a.body.dom.innerHTML);
  a.setContentSize(b.width, b.height)
};

var prepareReminderForm = function(f, obj, param, opts) {
	var remark = '';
	opts.each(function(o, k) {
		remark += decodeURIComponent(o).split('=')[1];
		if (k % 2 == 1) {
			remark += '<br>'
		} else {
			remark += ': ';
		}
	});
	var pm = Ext.select('*[rel=productsmodel' + param._id + ']').first();
	if (pm) {
		remark += 'Artikelnummer: ' + pm.dom.innerHTML;
	}
	var field = new Ext.form.Field({
		inputType : 'hidden',
		name : 'rmd_remark'
	});
	field.render(f.el);
	field.setValue(remark);
};
// Slider bei Whats new
function slideShow(conf) {
	var def = {
		Wrapper : Ext.get(conf.Wrapper || 'wrap_slider'),
		Elems : Ext.select('.' + (conf.Elems || 'slide_elements')),
		ElemsAll : conf.ElemsAll || {},
		Navigation : conf.Navigation || false,
		NavigationOutside : conf.NavigationOutside || false,
		Playable : conf.Playable || false,
		StopOnMouseOver : conf.StopOnMouseOver || false,
		Duration : conf.Duration || 500,
		Delay : conf.Delay || 3000,
		Type : conf.Type || 'left',
		Callback : conf.Callback || false
	};
	if (!def.Wrapper && (def.Callback !== false && (typeof def.Callback) == 'function')) {
		Ext.callback(def.Callback, this, def);
		return;
	} else if (!def.Wrapper && (def.Callback === false || (typeof def.Callback) != 'function')) {
		return;
	};
	def.Elems.ElemCounter = {
		active : 0,
		once : def.Elems.elements.length,
		twice : def.Elems.elements.length * 2
	};
	// Standard-Animation
	switch (conf.Animation) {
		case true :
			def.Animation = true;
			break;
		case 'default' :
			def.Animation = {
				duration : def.Duration / 1000,
				delay : 0
			};
			break;
		default :
			def.Animation = false;
			break;
	}
	if (def.Wrapper === null) {
		return;
	}
	if (def.Wrapper.dom.parentElement.id == '') {
		def.Wrapper.dom.parentElement.id = def.Wrapper.dom.id + '_slider_container';
	};
	def.Container = Ext.get(def.Wrapper.dom.parentElement.id);
	// Mehr Inhalt als anzeigbar?
	def.Elems.Width = 0;
	def.Elems.each(function(el) {
		def.Elems.Width += el.getWidth() + el.getMargins().left + el.getMargins().right;
	});
	// Inhalt passt in Anzeigebereich
	if (def.Container.getWidth() >= def.Elems.Width) {
		def.Navigation = false;
		def.Playable = false;
		def.Elems.each(function(el) {
			if (typeof (el.child('img')) != 'undefined') {
				el.addClass('original');
			};
		});
		// Inhalt passt nicht in Anzeigebereich
	} else {
		// Breite des Wrappers setzen
		def.Elems.each(function(el) {
			Ext.DomHelper.append(def.Wrapper, el.dom.outerHTML);
			if (typeof (el.child('img')) != 'undefined') {
				el.addClass('original');
			};
		});
		def.ElemsAll = Ext.select('.' + (conf.Elems || 'slide_elements'));
		def.ElemsAll.Width = def.Elems.Width * 2;
		def.Wrapper.setWidth(def.ElemsAll.Width, false);
	}
	// Initial-Position des Wrappers setzen
	def.Wrapper.defX = def.Wrapper.getX();
	def.Wrapper.defMaxX = def.Wrapper.getX() - def.Elems.Width;
	def.Wrapper.setX(def.Wrapper.defX, false);
	// Hat der Slider eine Navigation? --> Initiieren
	if (def.Navigation === true) {
		nav = {
			class : def.Container.dom.id + '_slider_nav',
			id : def.Container.dom.id + '_sln_',
			tpl : [
				'<a href="#" rel="nofollow" onclick="return (false);" class="',
				'" id="',
				'"></a>'
			],
			type : [
				'left',
				'right'
			]
		};
		// Ist die Navigation im Root-Element oder nicht?
		if (def.NavigationOutside === false) {
			Ext.DomHelper.append(def.Container, (nav.tpl[0] + nav.class + nav.tpl[1] + nav.id + nav.type[0] + nav.tpl[2]));
			Ext.DomHelper.append(def.Container, (nav.tpl[0] + nav.class + nav.tpl[1] + nav.id + nav.type[1] + nav.tpl[2]));
		} else if (def.NavigationOutside === true) {
			if (def.Container.dom.parentElement.id == '') {
				def.Container.dom.parentElement.id = 'parent_' + def.Container.dom.id;
			};
			def.pContainer = Ext.get(def.Container.dom.parentElement.id);
			def.pContainer.setPositioning({
				position : 'relative'
			});
			Ext.DomHelper.append(def.pContainer, (nav.tpl[0] + nav.class + nav.tpl[1] + nav.id + nav.type[0] + nav.tpl[2]));
			Ext.DomHelper.append(def.pContainer, (nav.tpl[0] + nav.class + nav.tpl[1] + nav.id + nav.type[1] + nav.tpl[2]));
		};
		def.Nav = Ext.select('.' + nav.class);
		def.Nav.each(function(el) {
			if (el.dom.id === (nav.id + nav.type[0])) {
				el.on('click', function() {
					def.Type = 'right';
					slideShowTo(def);
				});
			} else if (el.dom.id === (nav.id + nav.type[1])) {
				el.on('click', function() {
					def.Type = 'left';
					slideShowTo(def);
				});
			};
		});
	};
	// Ist der Slider abspielbar?
	if (def.Playable === true) {
		var slideInit = window.setInterval(function() {
			slideShowTo(def);
		}, def.Delay);
		if (def.StopOnMouseOver === true) {
			def.ElemsAll.each(function(el) {
				el.on('mouseover', function() {
					window.clearInterval(slideInit);
				});
				el.on('mouseout', function() {
					slideInit = window.setInterval(function() {
						slideShowTo(def);
					}, def.Delay);
				});
			});
			def.Nav.each(function(el) {
				el.on('mouseover', function() {
					window.clearInterval(slideInit);
				});
				el.on('mouseout', function() {
					slideInit = window.setInterval(function() {
						slideShowTo(def);
					}, def.Delay);
				});
			});
		}
	}
	if (def.Callback !== false && (typeof def.Callback) == 'function') {
		Ext.callback(def.Callback, this, def);
	};
	def.Wrapper.setVisible(true).setOpacity(1, true);
}
// Slide-Funktion
function slideShowTo(conf) {
	var def = conf;
	def.Elem = Ext.get(def.ElemsAll.elements[def.Elems.ElemCounter.active]);
	def.Elem.Width = def.Elem.getWidth() + def.Elem.getMargins().left + def.Elem.getMargins().right; // Gesamtbreite
	// aktuelles
	// Element
	// Slider nach links
	if (def.Type === 'left') {
		if (def.Elems.ElemCounter.active === def.Elems.ElemCounter.once) {
			def.Elems.ElemCounter.active = 0;
			def.Wrapper.setX(def.Wrapper.defX, false);
		}
		def.Wrapper.setX(def.Wrapper.getX() - def.Elem.Width, def.Animation);
		def.Elems.ElemCounter.active++;
		// Slider nach rechts
	} else if (def.Type === 'right') {
		if (def.Elems.ElemCounter.active === 0) {
			def.Elems.ElemCounter.active = def.Elems.ElemCounter.once;
			def.Wrapper.setX(def.Wrapper.defMaxX, false);
		}
		def.Wrapper.setX(def.Wrapper.getX() + def.Elem.Width, def.Animation);
		def.Elems.ElemCounter.active--;
	}
}
// Blendet Gr��enlink ein wenn Attributmatrix und Attribut Gr��e gesetzt ist
// productwrap_{c_id}
function groessenlink() {
	var conf = {
		container : '.product',
		attrMatrix : '.attrmatrix',
		sizeLink : '.ans_shop_detail_groessen'
	};
	if (Ext.select(conf.container + ' ' + conf.attrMatrix).elements[0] == undefined || Ext.select(conf.container + ' ' + conf.sizeLink).elements[0] == undefined) {
		return;
	};
	var attrContainer = Ext.select(conf.container);
	attrContainer.each(function(el) {
		var attrSize = Ext.select('#' + el.dom.id + ' ' + conf.attrMatrix + ' select[data-attr="Gr��e"]').first();
		var attrLink = Ext.select('#' + el.dom.id + ' ' + conf.sizeLink).first();
		if (attrSize != null && attrLink != null) {
			attrLink.setOpacity(100).setVisible('visible');
		};
	});
}
if (typeof Ext.Slider != 'undefined') {
	Ext.Slider.prototype.sliderImage = basepath + '__/images/2014/slider_horiz.png';
}
// Blendet die Box der Sortierfunktion ein/aus
function sortBox(trgt, trig) {
	var Box = Ext.get(trgt);
	var Trigger = Ext.get(trig);
	if (typeof Box != 'undefined' && typeof Trigger != 'undefined') {
		if (Box.hasClass('closed') !== true) {
			Box.setHeight(Box.getHeight());
			Box.addClass('closed');
		} else {
			Box.removeClass('closed');
		};
	};
};
// Grossansicht aus den Detailansichten
var makeItBig = function() {
	var addpics = Ext.select('.additionalpics:not(.slick-cloned) img[rel^=additionalpics]');
	if (addpics.getCount()) {
		var elObj = {
			tag : 'div',
			id : 'makeItBigDiv'
		};
		if (Ext.get(elObj.id)) {
			return;
		};
		var el = Ext.DomHelper.append(document.body, elObj, true);
		var elCnt = Ext.DomHelper.append(el, {
			tag : 'div',
			id : 'makeItBigDivCnt'
		}, true);
		el.setBox(Ext.select('body').first().getBox());
		elCnt.setBox(Ext.get('site').getBox());
		// el.setBox( Ext.get( 'site' ).getBox() ); /* war mal... */
		el.setStyle({
			background : 'rgba(0,0,0,.9)',
			zIndex : '10000',
			position : 'absolute',
			top : '0px'
		});
		var str_match = /thm\/med_/, csrc = Ext.get('productImage').dom.src;
		var current = 0;
		addpics.each(function(img, comp, c) {
			if (img.dom.src.replace(str_match, 'media/med_') == csrc) {
				current = c;
			}
		});
		var ci = addpics.elements[current];
		str_match = (ci.src.match(str_match)) ? str_match : /media\/med_/;
        var cDate   = new Date();
        var cDate   = cDate.format('Y');
		var html = '<div class="slideShowHaeder"><img src="' + basepath + '__/images/2014/mondelliLogoSlider.png"><img id="closeBtn" src="' + basepath + '__/images/2014/closebtn.png" style="right: 90px; cursor: pointer;"></div>';
		html += '<div id="slideList"></div><div id="slideDetails"><img id="bigPic" src="' + ci.src.replace(str_match, 'medium/med_') + '"><div id="slideText"></div><div id="slideLeft"></div><div id="slideRight"></div><span id="big_copyright">&copy; Copyright 2013-' + cDate + ' Mondelli-Lifestyle</span></div>';
		elCnt.dom.innerHTML = html;
		Ext.get('slideText').dom.innerHTML = '<h2>' + Ext.query('.ans_shop_detail_title').first().innerHTML + '</h2>' + Ext.query('.ans_shop_detail_teaser').first().innerHTML;
		addpics.each(function(img, comp, c) {
			var i = Ext.DomHelper.append('slideList', {
				tag : 'img',
				src : img.dom.src,
				id : 'pic' + String(c)
			}, true);
			i.on('click', function(ev, el) {
				Ext.get('bigPic').dom.src = el.src.replace(str_match, 'medium/med_');
				current = Number(el.id.replace(/pic/, ''));
			})
		});
		Ext.get('slideLeft').on('click', function() {
			current -= 1;
			if (current < 0) {
				current = addpics.getCount() - 1;
			}
			Ext.get('bigPic').dom.src = Ext.getDom('pic' + String(current)).src.replace(str_match, 'medium/med_');
		});
		Ext.get('slideRight').on('click', function() {
			current += 1;
			if (current > addpics.getCount() - 1) {
				current = 0;
			}
			Ext.get('bigPic').dom.src = Ext.getDom('pic' + String(current)).src.replace(str_match, 'medium/med_');
		});
		Ext.get('closeBtn').on('click', function() {
			Ext.get('makeItBigDiv').remove();
		});
	};
};
function copyInnerHTML(def) {
	var conf = {
		source : def.source || 'copy_source',
		target : def.target || 'copy_target',
		smooth : def.smooth || false,
		type : def.type || 'innerHTML'
	};
	conf.source = Ext.get(conf.source);
	conf.target = Ext.get(conf.target);
	if (conf.source && conf.target) {
		if (conf.type == 'innerHTML') {
			conf.target.dom.innerHTML = conf.source.dom.innerHTML;
		} else if (conf.type == 'outerHTML') {
			conf.target.dom.outerHTML = conf.source.dom.outerHTML;
		};
		if (conf.smooth === true) {
			conf.target.setOpacity(1, true);
		}
	}
}
function openFAQ(id, container) {
	var faqid = 'faq' + id;
	var linkid = 'faqlink' + id;
	var display_o = 'c_titlemi';
	var display_c = 'c_titlenomi';
	var el_cnt = (container && container != '') ? ('.' + container + ' ') : '';
	var el_displays = Ext.select(el_cnt + '.faqdisplays');
	var slideConf = {
		easing : 'easeIn',
		duration : 0.5,
		useDisplay : true
	};
	if (el_displays) {
		var el_trigger = '';
		// Custom-Shit... // Start
		var look = {
			faqid : '',
			linkid : ''
		};
		// Custom-Shit... // Ende
		el_displays.each(function(el) {
			el_trigger = faqid.replace(id, '');
			el_trigger = el.dom.id.replace(el_trigger, 'faqlink');
			// Custom-Shit... // Start
			if (el_trigger.match('look') && el.dom.id.match('look')) {
				look.faqid = el.dom.id;
				look.linkid = el_trigger;
			};
			// Custom-Shit... // Ende
			if (el.dom.id == faqid) {
				if (Ext.get(el).isDisplayed() == false) {
					slideConf.easing = 'easeIn';
					el.slideIn('t', slideConf);
					if (Ext.get(linkid).hasClass(display_c)) {
						Ext.get(linkid).replaceClass(display_c, display_o);
					};
				} else if (Ext.get(el).isDisplayed() == true) {
					slideConf.easing = 'easeOut';
					el.slideOut('t', slideConf);
					if (Ext.get(linkid).hasClass(display_o)) {
						Ext.get(linkid).replaceClass(display_o, display_c);
					};
				};
			} else {
				slideConf.easing = 'easeOut';
				el.isVisible() && el.slideOut('t', slideConf);
				if (Ext.get(el_trigger).hasClass(display_o)) {
					Ext.get(el_trigger).replaceClass(display_o, display_c);
				};
			};
		});
		// Custom-Shit... // Start
		if (faqid != look.faqid && Ext.get(linkid) && (Ext.get(linkid).hasClass(display_c) === true) && (Ext.get(look.linkid) && Ext.get(look.linkid).hasClass(display_c) === true)) {
			slideConf.easing = 'easeIn';
			Ext.get(look.faqid).slideIn('t', slideConf);
			if (Ext.get(look.linkid).hasClass(display_c)) {
				Ext.get(look.linkid).replaceClass(display_c, display_o);
			};
		};
		// Custom-Shit... // Ende
	};
};
function boxWishlist() {
	msgBox('Achtung', 'Um diese Funktion nutzen zu k&ouml;nnen, m&uuml;ssen Sie sich erst anmelden/registrieren', 'ok', function() {
		extendLogin('lh_login');
	})
};
function unifyEls(elClass) {
	if (elClass.search(/^\./) == -1) {
		elClass = '.' + elClass;
	};
	var els = Ext.select(elClass);
	if (els.getCount() > 0) {
		els.each(function(el) {
			Ext.id(el);
		});
		return els;
	} else {
		return [];
	};
};
function shiftSlider(config) {
	var def = {
		cID : config.cID || 'shift_slider',
		cWidth : config.cWidth || 980,
		cHeight : config.cHeight || 420,
		elCnt : config.elCnt || 'shift_box',
		elHeads : config.elHeads || 'title_teaser',
		elThumbs : config.elThumbs || 'thumb',
		initDelay : config.initDelay || 1000,
		playDelay : config.playDelay || 3000,
		autoPlay : config.autoPlay || false,
		autoPlaySlide : config.autoPlaySlide || false,
		pauseMOver : config.pauseMOver || false,
		navControl : config.navControl || false,
		scrollDir : config.scrollDir || 'down',
		scrollAnim : config.scrollAnim || false
	};
	def.cID = Ext.get(config.cID);
	def.elCnt = Ext.select('#' + def.cID.dom.id + ' .' + def.elCnt).first();
	def.elHeads = Ext.select('#' + def.cID.dom.id + ' .' + def.elHeads);
	def.elThumbCls = def.elThumbs;
	def.elThumbs = Ext.select('#' + def.cID.dom.id + ' .' + def.elThumbs);
	def.elBox = {
		tag : 'div',
		class : 'ttBox',
		id : 'ttBox_' + def.cID.dom.id,
		style : 'display:inline-block;'
	};
	if (def.cID && def.elCnt && def.elHeads.getCount() > 0 && def.elThumbs.getCount() > 0) {
		def.elCnt.createChild(def.elThumbs.first().dom.outerHTML);
		def.elThumbs = Ext.select('#' + def.cID.dom.id + ' .' + def.elThumbCls);
		def.gID = 'shiftSlider_' + def.cID.dom.id;
		var elCounter = 0;
		function handleCls(elEm, elEv, elCls) {
			var elem = {
				elEm : Ext.get(elEm),
				elEv : elEv,
				elCls : elCls
			};
			elem.elEm.on(elem.elEv, function() {
				def.elHeads.each(function(el) {
					if (el.dom.id != elem.elEm.dom.id && el.hasClass('active') === true) {
						el.removeClass('active');
					};
				});
				switch (elem.elEv) {
					case 'mouseover' :
						if (elem.elEm.hasClass(elem.elCls) === false) {
							elem.elEm.addClass(elem.elCls);
						}
						;
						break;
					case 'mouseout' :
						if (elem.elEm.hasClass(elem.elCls) === true) {
							elem.elEm.removeClass(elem.elCls);
						}
						;
						break;
					case 'click' :
						if (elem.elEm.hasClass(elem.elCls) === false) {
							elem.elEm.addClass(elem.elCls);
						}
						;
						break;
					case 'touchstart' :
						if (elem.elEm.hasClass(elem.elCls) === false) {
							elem.elEm.addClass(elem.elCls);
						}
						;
						break;
					default :
						break;
				};
			});
		};
		function handleNav(elEm) {
			var navCtrl = Ext.get(elEm);
			navCtrl.on('mouseover', function() {
				window[def.gID].config.actEl = parseInt(navCtrl.dom.id.split('_').last(), 10);
				var thHeight = def.elThumbs.item(window[def.gID].config.actEl).getY();
				var cnHeight = def.elCnt.getY();
				var slHeight = def.cID.getY();
				if (thHeight > cnHeight) {
					def.elCnt.setY(cnHeight - thHeight + slHeight, def.scrollAnim);
				} else if (thHeight == cnHeight) {
					def.elCnt.setY(def.cID.getY(), def.scrollAnim);
				};
				if (def.autoPlaySlide === false) {
					def.cID.addClass('autoPlaySlide');
				};
			});
			navCtrl.on('mouseout', function() {
				if (def.autoPlaySlide === false) {
					def.cID.removeClass('autoPlaySlide');
				};
			});
		};
		window['shiftScroll'] = function() {
			var ssDef = window[def.gID].config;
			ssDef.actHead = ssDef.elHeads.item(ssDef.actEl);
			if (ssDef.scrollDir == 'up') {
				if (ssDef.actEl == 0) {
					ssDef.actEl = ssDef.elThumbs.getCount();
					ssDef.elCnt.setY(ssDef.cID.getY() - ssDef.elThumbs.item(ssDef.actEl - 1).getY(), false);
					ssDef.actEl--;
					ssDef.actHead = ssDef.elHeads.item(0);
				};
				ssDef.elCnt.setY(ssDef.elCnt.getY() - ssDef.elThumbs.item(ssDef.actEl - 1).getY() + ssDef.cID.getY(), def.scrollAnim);
				ssDef.actHead.removeClass('active');
				ssDef.actEl--;
				if (ssDef.actEl <= ssDef.elHeads.getCount()) {
					ssDef.actHead = ssDef.elHeads.item(ssDef.actEl);
				} else {
					ssDef.actHead = ssDef.elHeads.item(0);
				};
			} else if (ssDef.scrollDir == 'down') {
				if ((ssDef.actEl + 1) == ssDef.elThumbs.getCount()) {
					ssDef.elCnt.setY(ssDef.cID.getY(), false);
					ssDef.actEl = 0;
					ssDef.actHead = ssDef.elHeads.item(ssDef.actEl);
				};
				ssDef.elCnt.setY(ssDef.elCnt.getY() - ssDef.elThumbs.item(ssDef.actEl + 1).getY() + ssDef.cID.getY(), def.scrollAnim);
				ssDef.actHead.removeClass('active');
				ssDef.actEl++;
				if (ssDef.actEl >= ssDef.elHeads.getCount()) {
					ssDef.actHead = ssDef.elHeads.item(0);
				} else {
					ssDef.actHead = ssDef.elHeads.item(ssDef.actEl);
				};
			};
			ssDef.actHead.addClass('active').addClass('active');
			window[ssDef.gID].config = ssDef;
			window.clearTimeout(window[def.gID].timer);
			window[ssDef.gID].timer = window.setTimeout("shiftScroll()", ssDef.playDelay);
		};
		def.cID.setWidth(def.cWidth).setHeight(def.cHeight);
		Ext.DomHelper.append(def.cID, def.elBox);
		def.elHeads.appendTo(def.elBox.id);
		def.elThumbs.setWidth(def.cWidth).setHeight(def.cHeight);
		elCounter = 0;
		def.elHeads.each(function(el) {
			el.dom.id = 'slide_head_' + elCounter;
			handleCls(el.dom.id, 'mouseover', 'active');
			handleCls(el.dom.id, 'mouseout', 'active');
			handleCls(el.dom.id, 'click', 'active');
			handleCls(el.dom.id, 'touchstart', 'active');
			if (def.navControl === true) {
				handleNav(el.dom.id);
			};
			elCounter++;
		});
		elCounter = 0;
		def.elThumbs.each(function(el) {
			el.dom.id = 'slide_thumb_' + elCounter;
			elCounter++;
		});
		window[def.gID] = {};
		window[def.gID].config = def;
		window[def.gID].config.actEl = 0;
		window[def.gID].config.actHead = def.elHeads.item(window[def.gID].config.actEl);
		window[def.gID].config.actHead.addClass('active');
		def.cID.setVisible(true).setOpacity(1, true);
		if (def.autoPlay === true) {
			if (def.autoPlaySlide === true) {
				def.cID.addClass('autoPlaySlide');
			};
			window[def.gID].timer = window.setTimeout("shiftScroll()", def.initDelay);
			if (def.pauseMOver === true) {
				def.cID.on('mouseover', function() {
					window.clearTimeout(window[def.gID].timer);
				});
				def.cID.on('mouseout', function() {
					window[def.gID].timer = window.setTimeout("shiftScroll()", def.initDelay);
				});
			};
		};
	};
};
// sessionStorage Verwaltung
function sStore(data, type) {
	var def = {
		name : data.name || false,
		content : data.content || ''
	};
	var ret = false;
	if (def.name !== false) {
		switch (type) {
			case 'set' :
				def.content = Ext.util.JSON.encode(def.content);
				window.sessionStorage.setItem(def.name, def.content);
				ret = true;
				break;
			case 'get' :
				if (window.sessionStorage.getItem(def.name) == null) {
					ret = false;
				} else {
					ret = window.sessionStorage.getItem(def.name);
					ret = Ext.util.JSON.decode(ret, true);
				}
				;
				break;
			case 'remove' :
				window.sessionStorage.removeItem(def.name);
				if (window.sessionStorage.getItem(def.name) == null) {
					ret = true;
				} else {
					ret = false;
				}
				;
				break;
			default :
				break;
		};
	} else {
		if (type == 'clear') {
			window.sessionStorage.clear();
			if (window.sessionStorage.length == 0) {
				ret = true;
			};
		};
	};
	return ret;
};
function hisBack() {
	window.history.back(); // Test-Hack: Scheinbar sind die urspr�nglichen Probleme mit der History im IE11 obsolet
	return;
	var sObj = {
		name : 'history',
		content : ''
	};
	if (sStore(sObj, 'get') === false) {
		return window.history.back();
	} else {
		var thisHistory = sStore(sObj, 'get');
		if (thisHistory.length > 0) {
			thisHistory.remove(thisHistory[thisHistory.length - 1]);
			sObj.content = thisHistory;
			sStore(sObj, 'set');
			return window.location.href = thisHistory.last();
		} else {
			return window.history.back();
		};
	};
};
function buildHistory(hisLength) {
	var sObj = {
		name : 'history',
		content : ''
	};
	if (sStore(sObj, 'get') === false) {
		sObj.content = [
			window.location.href
		];
		sStore(sObj, 'set');
	} else {
		var thisHistory = sStore(sObj, 'get');
		if (thisHistory.last() != window.location.href) {
			thisHistory[thisHistory.length] = window.location.href;
			if ((thisHistory.length - 1) == hisLength) {
				thisHistory.remove(thisHistory[0]);
			};
			sObj.content = thisHistory;
			sStore(sObj, 'set');
		};
	};
};
function scrollToTop(config) {
	var conf = {
		appendTo : config.appendTo || false,
		scrollTop : config.scrollTop || false,
		scrollTrigger : config.scrollTrigger || false,
		mobileSimple : config.mobileSimple || false
	};
	var atEl = Ext.get(conf.appendTo);
	var scEl = Ext.get(conf.scrollTop);
	if (atEl != null && scEl != null) {
		if (atEl.getStyle('position') == 'static') {
			atEl.setStyle({
				position : 'relative'
			});
		}
		var toTop = {
			tag : 'a',
			id : Ext.id(),
			class : 'scrolltotop',
			style : 'visibility:hidden;opacity:0;',
			href : '#',
            rel   : 'nofollow',
			title : 'Zur�ck nach oben',
			html : 'Zur�ck nach oben'
		};
		Ext.DomHelper.append(atEl, toTop);
		var dScrollEl = (Ext.isChrome === true || Ext.isSafari === true || Ext.isOpera === true) ? window.document.body : window.document.documentElement;
		var dClientHeight = 0;
		var dScrollTop = 0;
		function setDims() {
			dClientHeight = window.document.documentElement.clientHeight;
			dScrollTop = dScrollEl.scrollTop;
		};
		if (conf.scrollTrigger === true) {
			setDims();
			Ext.get(toTop.id).setY(dScrollTop + dClientHeight - Ext.get(toTop.id).getHeight(), false);
			if (conf.mobileSimple === true && isTouch() === true) {
				Ext.get(toTop.id).setStyle({
					position : 'fixed',
					top : 'auto',
					right : '20px',
					bottom : '0px',
					left : 'auto'
				});
			} else {
				window.onscroll = function() {
					setDims();
					Ext.get(toTop.id).setY(dScrollTop + dClientHeight - Ext.get(toTop.id).getHeight(), false);
				};
			};
		};
		Ext.get(toTop.id).setVisible(true).setOpacity(0.5, true);
		Ext.get(toTop.id).on('click', function() {
			Ext.get(dScrollEl).scrollTo('top', scEl.getY(), true);
		}, undefined, {
			preventDefault : true
		});
	};
};
function rewriteTimepopup(config) {
	if (config.msgDLG.buttons && config.msgDLG.buttons.length > 0 && config.msgDLG.buttons[0].text == 'OK' && config.msgScope.msgClass == 'newsletterregistrierung') {
		config.msgDLG.buttons[0].el.select('button.x-btn-text').first().update('Sp�ter registrieren');
		if (config.msgDLG.el.select('input.donotshow').first()) {
			config.msgDLG.el.select('input.donotshow').first().on('change', function(ev, trigger) {
				if (trigger.checked && trigger.checked === true) {
					this.hide();
				};
			}, config.msgDLG);
		};
	};
	return config.msgDLG;
};
function trimBrandNav(config) {
	var Elements = Ext.select(config.path);
	var Range = config.range;
	var Counter = 0;
	if (Elements.elements.length && (Elements.elements.length > 0) && (Range.length == 2) && (Range[0] < Range[1])) {
		Elements.each(function(el) {
			if (Counter < Range[0] || Counter > Range[1] && (Counter < (Elements.elements.length - 1))) {
				el.remove();
			}
			Counter++;
		});
	};
};
var isTouch = function() {
	var ret = false;
	if (Ext.select('html.touch').first()) {
		ret = true;
	};
	return ret;
};
function dynMobileMenu(config) {
	var def = {
		class : config.class || 'dropnav',
		serveLink : config.serveLink || false,
		serveRange : config.serveRange || false,
		isTouch : config.isTouch || false,
		pointEvent : 'click'
	};
	if (def.serveRange != false && def.serveRange.length && def.serveRange.length == 2) {
		def.serveRange = [
			parseInt(def.serveRange[0]),
			parseInt(def.serveRange[1])
		];
		if ((def.serveRange[0] > def.serveRange[1]) || (def.serveRange[1] < def.serveRange[0])) {
			def.serveRange = false;
		};
	};
	if (Ext.select('.' + def.class + 'UL').first()) {
		if (def.isTouch) {
			Ext.select('.' + def.class + 'UL').each(function(el) {
				if (!el.dom.id || el.dom.id == '') {
					Ext.id(el);
					def.id = el.dom.id;
				};
				def.pointEvent = 'touchstart';
				if ((def.serveRange == false || ((def.serveRange[0] < Ext.select('body').first().getWidth()) && (def.serveRange[1] > Ext.select('body').first().getWidth())))) {
					def.unmake = false;
					makeMobile(el, def);
				} else if (def.serveRange != false && ((def.serveRange[0] > Ext.select('body').first().getWidth()) || (def.serveRange[1] < Ext.select('body').first().getWidth()))) {
					def.unmake = true;
					makeMobile(el, def);
				};
			});
			Ext.select('ul.' + config.class + 'UL>li').replaceClass('li_' + config.class + 'a', 'li_' + config.class);
			Ext.select('ul.' + config.class + 'UL>li>a').replaceClass(config.class + 'a', config.class);
		} else {
			return;
		};
	};
	function makeMobile(elem, config) {
		elem.select('>li').each(function(el) {
			if (el.child('ul') && el.child('a')) {
				if (config.serveLink === true) {
					var closeButton = {
						tag : 'a',
						href : '#',
						id : 'cls_' + el.child('ul').dom.id,
						class : 'close',
						style : 'position:absolute;top:5px;right:5px;z-index:1;display:block;width:17px;height:17px;line-height:17px;padding:3px;font-weight:bold;text-align:center;color:#ffffff;background-color:#000000;',
						html : 'x'
					};
					Ext.DomHelper.append(el.child('ul'), closeButton);
				};
				makeMobileTouch(el.child('a'), config);
			};
		});
	};
	function makeMobileTouch(elem, config) {
		if (config.unmake === false) {
			elem.on(config.pointEvent, function() {
				if (this.hasClass(config.class)) {
					Ext.select('ul.' + config.class + 'UL>li').replaceClass('li_' + config.class + 'a', 'li_' + config.class);
					Ext.select('ul.' + config.class + 'UL>li>a').replaceClass(config.class + 'a', config.class);
					this.up('li').replaceClass('li_' + config.class, 'li_' + config.class + 'a');
					this.replaceClass(config.class, config.class + 'a');
				} else {
					Ext.select('ul.' + config.class + 'UL>li').replaceClass('li_' + config.class + 'a', 'li_' + config.class);
					Ext.select('ul.' + config.class + 'UL>li>a').replaceClass(config.class + 'a', config.class);
					if (config.serveLink === true) {
						document.location.href = elem.dom.href;
					};
				};
			}, undefined, {
				preventDefault : true
			});
			var closeBT = elem.up('li').select('a#cls_' + elem.up('li').select('>ul').first().dom.id).first();
			if (closeBT) {
				closeBT.on(config.pointEvent, function() {
					Ext.select('ul.' + config.class + 'UL>li').replaceClass('li_' + config.class + 'a', 'li_' + config.class);
					Ext.select('ul.' + config.class + 'UL>li>a').replaceClass(config.class + 'a', config.class);
				}, undefined, {
					preventDefault : true
				});
			};
		} else if (config.unmake === true) {
			elem.removeAllListeners();
		};
	};
};
var setThingsVisible = function() {
	var bsc = Ext.get('bookmarked_sites_count');
	if (bsc) {
		if (bsc.dom.innerHTML != '0') {
			bsc.setDisplayed('inline');
		} else {
			bsc.setDisplayed(false);
		}
	}
	var bcc = Ext.get('basket_count_content_shopcoupons');
	if (bcc) {
		if (bcc.dom.innerHTML != '0') {
			bcc.setDisplayed('inline');
		} else {
			bcc.setDisplayed(false);
		}
	}
};
var collectData = {
    data    : {},
    provider: {},
    grab    : function(type) {
        type    = (typeof type !== 'string') ? '' : '="' + type + '"';
        Ext.select('[data-grab' + type + ']').each(function(el, els, int) {
            el.select('[data-push]').each(function(el2, els2, int2) {
                if(int2 === 0) {
                    if(!collectData.data.hasOwnProperty(this.dom.dataset.grab)) {
                        collectData.data[this.dom.dataset.grab] = [];
                    };
                    collectData.data[this.dom.dataset.grab][int]   = {};
                };
                if(-1 === el2.dom.dataset.push.search(/\:/)) {
					if (typeof collectData.data[this.dom.dataset.grab][int][el2.dom.dataset.push] !== "undefined") {
						collectData.data[this.dom.dataset.grab][int][el2.dom.dataset.push] += "|" + el2.dom.innerHTML;
					} else {
						collectData.data[this.dom.dataset.grab][int][el2.dom.dataset.push] = el2.dom.innerHTML;
					}
                } else {
					if(typeof collectData.data[this.dom.dataset.grab][int][el2.dom.dataset.push.split(/\:/)[0]] !== "undefined") {
						collectData.data[this.dom.dataset.grab][int][el2.dom.dataset.push.split(/\:/)[0]] += "|" + el2.dom.dataset.push.split(/\:/)[1];
					} else {
						collectData.data[this.dom.dataset.grab][int][el2.dom.dataset.push.split(/\:/)[0]] = el2.dom.dataset.push.split(/\:/)[1];
					}
                };
            }, el);
        });
    },
    integrate   : function(obj) {
        if(obj.hasOwnProperty('provider') && obj.hasOwnProperty('type') && collectData.data.hasOwnProperty(obj.type) && collectData.data[obj.type].length > 0 && obj.hasOwnProperty('fields')) {
            if(!collectData.provider.hasOwnProperty('provider')) {
                collectData.provider[obj.provider]  = {};
            };
            if(!collectData.provider.hasOwnProperty('type')) {
                collectData.provider[obj.provider][obj.type]  = [];
            };
            collectData.data[obj.type].each(function(el, int) {
                if(!collectData.provider[obj.provider][obj.type][int]) {
                    collectData.provider[obj.provider][obj.type][int]   = {};
                };
                for(attr in obj.fields) {
                    if(el.hasOwnProperty(obj.fields[attr])) {
                        collectData.provider[obj.provider][obj.type][int][attr] = el[obj.fields[attr]];
                    }
                }
            });
        }
    },
    edit        : function(type, field, fnc) {
        var els = Ext.select('*[data-grab="' + type + '"] *[data-push="' + field + '"]');
        if(els.getCount() > 0 && typeof fnc === 'function') {
            fnc(els);
        };
    }
};
collectData.init    = function(type, call) {
    if(typeof call === 'object' && typeof call.precall === 'function') {
        call.precall();
    };
    collectData.grab(type);
    if(typeof call === 'object' && typeof call.recall === 'function') {
        call.recall();
    };
};
recon.on('bookmarkCountChanged', function(el, param) {
	if (param.count == 0) {
		el.setDisplayed(false);
	} else {
		el.setDisplayed('inline');
	}
});
recon.on('basketCountChanged', function(el, c) {
	if (Number(c) == 0) {
		el.setDisplayed(false);
	} else {
		el.setDisplayed('inline');
	}
});
recon.on('foundWordClick', function(el, word) {
    if('undefined' === typeof word) {
        if(Ext.select('.spsearch').first()) {
            word    = Ext.select('.spsearch input[type="text"]').first().dom.value;
        } else {
            word    = '';
        }
    }
    if(2 < word.length) {
        document.location.href = '/suche/index.html?q=' + word;
    }
	return (false);
});
recon.on('afterMsgBoxSlide', function(title, text, link, dlg) {
        collectData.init('basket', {
            precall : function() {
			},
            recall  : function() {
                    collectData.integrate({
                        provider: 'kupona',
                        type    : 'basket',
                        fields  : {
                            kp_shoppingcart_product_ids  : 'productsmodel'}});
                if(collectData.provider.hasOwnProperty('kupona')) {
                    for(type in collectData.provider.kupona) {
                        for(var int = 0; int < collectData.provider.kupona[type].length; int++) {
                            for(attr in collectData.provider.kupona[type][int]) {
                                if('kp_shoppingcart_product_ids' === attr) {
                                    if(window[attr]) {
                                        window[attr].push(collectData.provider.kupona[type][int][attr]);
                                    } else {
                                        window[attr]    = [];
                                        window[attr].push(collectData.provider.kupona[type][int][attr]);
                                    }
                                    if(int === collectData.provider.kupona[type].length - 1) {
                                        window[attr]    = window[attr].join(',');
                                    }
                                } else {
                                    window[attr]    = collectData.provider.kupona[type][int][attr];
                                }
                            }
                        }
                    }
                    setScript('kupona', 'https://d31bfnnwekbny6.cloudfront.net/customers/29054.min.js');
                }
            }
        });
});
if('function' != typeof debugrecon) {
    var debugrecon = function(obj) {
        var cookie  = document.cookie.split(/\; /);
        if(cookie && cookie.length > 0) {
            if(-1 == cookie.indexOf('debugrecon=1')) {
                console.log('debugging deactivated');
            } else {
                switch(typeof obj) {
                    case 'function':
                        obj();
                    break;
                    default:
                        console.log(obj);
                    break;
                }
            }
        }
    }
}
function setScript(id, src, overwrite) {
    var ret = false;
    var prfx    = 'script_';
    if(!Ext.get(prfx + id) || (Ext.get(prfx + id) && true === overwrite)) {
        if(Ext.get(prfx + id)) {
            Ext.get(prfx + id).remove();
        }
        var nScript = document.createElement('script');
        nScript.setAttribute('id', prfx + id);
        nScript.setAttribute('type', 'text/javascript');
        nScript.setAttribute('src', src);
        Ext.select('body').first().dom.appendChild(nScript);
        ret = true;
    } else {
        ret = false;
    }
    return ret;
}
Ext.onReady(setThingsVisible);
recon.on('afterUpdatePanel', function(type, scope, el) {
    if('booke' === type) {collectData.init('wishlist', {
            precall : function() {
			},
            recall  : function() {
                    collectData.integrate({
                        provider: 'kupona',
                        type    : 'wishlist',
                        fields  : {
                            kp_wishlist_product_ids  : 'productsmodel'}});
                if(collectData.provider.hasOwnProperty('kupona')) {
                    for(type in collectData.provider.kupona) {
                        for(var int = 0; int < collectData.provider.kupona[type].length; int++) {
                            for(attr in collectData.provider.kupona[type][int]) {
                                if('kp_wishlist_product_ids' === attr) {
                                    if(window[attr]) {
                                        window[attr].push(collectData.provider.kupona[type][int][attr]);
                                    } else {
                                        window[attr]    = [];
                                        window[attr].push(collectData.provider.kupona[type][int][attr]);
                                    }
                                    if(int === collectData.provider.kupona[type].length - 1) {
                                        window[attr]    = window[attr].join(',');
                                    }
                                } else {
                                    window[attr]    = collectData.provider.kupona[type][int][attr];
                                }
                            }
                        }
                    }
                    setScript('kupona', 'https://d31bfnnwekbny6.cloudfront.net/customers/29054.min.js');
                }
            }
        });
    }
});
var attrAutoSelect  = {
    init    : function() {
        Ext.select('.attrmatrix').each(function(el) {
            el.select('select[rel="attributematrix"]').each(function(el2) {
                if(3 > el2.dom.length && true != el2.dom.item(1).disabled) {
                    el2.dom.selectedIndex    = 1;
                    configureOptions(false, this.id);
                }
            }, el.dom);
        });
    },
    aAjax : function(attrID) {
        Ext.select('#attrmatrix_' + attrID + ' select[rel="attributematrix"]').each(function(el) {
            if(0 == el.dom.selectedIndex && 3 > el.dom.length && true != el.dom.item(1).disabled) {
                el.dom.selectedIndex    = 1;
                configureOptions(false, this.id);
            }
        }, Ext.get('attrmatrix_' + attrID).dom);
    }
};
var serviceBox  = {
    init    : function(boxID, triggerID) {
        if(Ext.get(boxID) && Ext.get(triggerID) && Ext.get(boxID).select('.service_head').first()) {
            Ext.get(boxID).dom.dataset.trigger  = triggerID;
            if(Ext.select('html.touch').first()) {
                Ext.get(boxID).select('.service_head').first().on('click', function(ev, el) {
                    this.toggleClass('hidden');
                    if(Ext.get(this.dom.dataset.trigger).hasClass('hidden')) {
                        Ext.get(this.dom.dataset.trigger).removeClass('hidden');
                    }
                }, Ext.get(boxID));
                Ext.get(triggerID).on('click', function(ev, el) {
                    Ext.get(el).toggleClass('hidden');
                    this.toggleClass('hidden');
                }, Ext.get(boxID));
            }
            if(Ext.select('html.no-touch').first()) {
                Ext.get(boxID).on('mouseout', function(ev, el) {
                    if(this.contains(ev.browserEvent.toElement)) {
                        return;
                    }
                    if(!this.hasClass('hidden')) {
                        this.addClass('hidden');
                    }
                    if(Ext.get(this.dom.dataset.trigger).hasClass('hidden')) {
                        Ext.get(this.dom.dataset.trigger).removeClass('hidden');
                    }
                }, Ext.get(boxID));
                Ext.get(triggerID).on('mouseover', function(ev, el) {
                    if(this.hasClass('hidden')) {
                        this.removeClass('hidden');
                    }
                    if(!Ext.get(el).hasClass('hidden')) {
                        Ext.get(el).addClass('hidden');
                    }
                }, Ext.get(boxID));
            }
        }
    }
};
var fControl    = {
    def     : {},
    init    : function(id) {
        if('string' === typeof id && 0 < id.length) {
            this.def.id   = id;
            this.def.form = Ext.get(this.def.id).select('form').first();
        }
        if(Ext.get(id) && getCookie(id)) {
            window.scrollTo(window.pageXOffset, Ext.get(id).dom.offsetTop + 200);
            if(true || Ext.get(id).select('.success').first()) {
                this.delData(id);
            }
        } else if(!Ext.get(id) && getCookie(id)) {
            this.delData(id);
        }
        if(this.def.hasOwnProperty('form') && null != this.def.form) {
            if('' != getCookie(id)) {
                var fData = this.getData(id);
            }
            Ext.get(this.def.form).on('submit', function(ev, el) {
                this.setData(el);
            }, this);
        }
    },
    delData : function(cookieID) {
        setCookie(cookieID, '', (new Date(2000, 0, 2)));
    },
    setData : function(formEl) {
        var data = [];
        for(var int = 0; int < formEl.length; int++) {
            if('formcaptcha' != formEl[int].name && 'hidden' != formEl[int].type && 'submit' != formEl[int].type) {
                data[int]   = {};
                data[int].name  = formEl[int].name;
                if('checkbox' === formEl[int].type) {
                    data[int].value  = (formEl[int].checked === true) ? true : false;
                } else {
                    data[int].value  = formEl[int].value;
                }
            }
        }
        data    = Ext.encode(data);
        data    = encode_utf8(data);
        data    = Base64.encode(data);
        setCookie(this.def.id, data);
    },
    getData : function(cookieID) {
        var data = false;
        try {
            data    = getCookie(cookieID);
            data    = decodeURIComponent(data);
            data    = Base64.decode(data);
            //data    = decode_utf8(data);
            data    = Ext.decode(data);
        } catch(ex) {
            console.log('decoding failed');
            console.log(ex);
        }
        var fField;
        if(data && 0 < data.length) {
            for(var int = 0; int < data.length; int++) {
                if(this.def.form.select('[name="' + data[int].name + '"]').first()) {
                    fField = this.def.form.select('[name="' + data[int].name + '"]').first();
                    if('checkbox' === fField.dom.type) {
                        fField.dom.checked = data[int].value;
                    } else {
                        fField.dom.value = data[int].value;
                    }
                }
            }
        }
    }
};
recon.on('configureOptions', function(obj, param, opts) {
    attrAutoSelect.aAjax(param._id);
});
runOnLoad(function() {
    serviceBox.init('service_box', 'service_icon');
    if('www.mondelli-studio.de' == document.location.host) {
        fControl.init('angebot_form');
    }
        collectData.init(undefined, {
            precall : function() {
                collectData.edit('category', 'href', function(els) {
                    els.each(function(el) {
                        el.dom.innerHTML    = document.location.href;
                    });
                });
                collectData.edit('product', 'href', function(els) {
                    els.each(function(el) {
                        el.dom.innerHTML    = document.location.protocol + '//' + document.location.host + el.dom.innerHTML;
                    });
                });
                collectData.edit('product', 'host', function(els) {
                    els.each(function(el) {
                        el.dom.innerHTML    = document.location.protocol + '//' + document.location.host;
                    });
                });
                collectData.edit('checkout', 'product_ids', function(els) {
                    els.each(function(el) {
                        el.dom.innerHTML    = el.dom.innerHTML.replace(/\|/g, ',');
                    });
                });
                collectData.edit('checkout', 'product_productmodels', function(els) {
                    els.each(function(el) {
                        el.dom.innerHTML    = el.dom.innerHTML.replace(/\|/g, ',');
                    });
                });
			},
            recall  : function() {
/*                collectData.integrate({
                    provider: 'affilinet',
                    type    : 'landingpage',
                    fields  : {
                        site            : 'provider_id'
                    }});
                collectData.integrate({
                    provider: 'affilinet',
                    type    : 'checkout',
                    fields  : {
                        site            : 'provider_id',
                        order_id        : 'shoporder',
                        order_total     : 'pricetotalvalue',
                        currency        : 'currency',
                        order_productids: 'product_ids',
                        order_prices    : 'product_prices',
                        order_quantity  : 'product_quantities'
                    }});
                collectData.integrate({
                    provider: 'affilinet',
                    type    : 'category',
                    fields  : {
                        site            : 'provider_id',
                        product_category: 'tree',
                        product_clickUrl: 'href',
                    }});
                collectData.integrate({
                    provider: 'affilinet',
                    type    : 'product',
                    fields  : {
                        site            : 'provider_id',
                        product_id      : 'productsmodel',
                        product_name    : 'title',
                        product_price   : 'price',
                        product_oldprice: 'baseprice',
                        product_category: 'tree',
                        product_brand   : 'brand',
                        product_inStock : 'available',
                        product_onsale  : 'discount_price',
                        currency        : 'currency',
                        product_clickUrl: 'href',
                        product_imgUrl  : 'thumbmediaurl'
                    }});*/
                    collectData.integrate({
                        provider: 'kupona',
                        type    : 'checkout',
                        fields  : {
                            kp_order_id                 : 'shoporder',
                            kp_order_total              : 'pricetotalvalue',
                            kp_order_product_ids        : 'product_productmodels',
                            kp_order_product_quantities : 'product_quantities'}});
                    collectData.integrate({
                        provider: 'kupona',
                        type    : 'basket',
                        fields  : {
                            kp_shoppingcart_product_ids  : 'productsmodel'}});
                    collectData.integrate({
                        provider: 'kupona',
                        type    : 'wishlist',
                        fields  : {
                            kp_wishlist_product_ids  : 'productsmodel'}});
                collectData.integrate({
                    provider: 'kupona',
                    type    : 'product',
                    fields  : {
                        kp_product_id               : 'productsmodel',
                        kp_category_id              : 'tree',
                        kp_product_brand            : 'brand',
                        kp_recommended_product_ids  : 'recommended'
                    }});
                if(collectData.provider.hasOwnProperty('affilinet')) {
                    for(type in collectData.provider.affilinet) {
                        if(collectData.provider.affilinet[type].length == 1) {
                            for(attr in collectData.provider.affilinet[type][0]) {
                                if('product_clickUrl' === attr || 'product_imgUrl' === attr) {
                                    collectData.provider.affilinet[type][0][attr] = encodeURIComponent(collectData.provider.affilinet[type][0][attr]);
                                }
                                if('product_onsale' === attr && collectData.provider.affilinet[type][0][attr] === '0.00') {
                                    delete collectData.provider.affilinet[type][0][attr];
                                }
                                window[attr]    = collectData.provider.affilinet[type][0][attr];
                            }
                        }
                    }
                    setScript('affilinet', 'https://partners.webmasterplan.com/art/JS/param.aspx');
                }
                if(collectData.provider.hasOwnProperty('kupona')) {
                    for(type in collectData.provider.kupona) {
                        for(var int = 0; int < collectData.provider.kupona[type].length; int++) {
                            for(attr in collectData.provider.kupona[type][int]) {
                                if('kp_wishlist_product_ids' === attr || 'kp_shoppingcart_product_ids' === attr) {
                                    if(window[attr]) {
                                        window[attr].push(collectData.provider.kupona[type][int][attr]);
                                    } else {
                                        window[attr]    = [];
                                        window[attr].push(collectData.provider.kupona[type][int][attr]);
                                    }
                                    if(int === collectData.provider.kupona[type].length - 1) {
                                        window[attr]    = window[attr].join(',');
                                    }
                                } else {
                                    window[attr]    = collectData.provider.kupona[type][int][attr];
                                }
                            }
                        }
                    }
                }
                //setScript('kupona', 'https://d31bfnnwekbny6.cloudfront.net/customers/29054.min.js');
            }
        });
	trimBrandNav({
		path : '.headmUL .subnav_wrap.tr_id9.brands .listing>ul.navsub li.first>ul.navsub>li',
		range : [
			0,
			8
		]
	});
	buildHistory(10);
	window.onhashchange = function() {
		buildHistory(10);
	};
	if (Ext.select('body').first().hasClass('tree_parent_0') === false && Ext.select('body').first().hasClass('tree_8') === false && Ext.select('body').first().hasClass('tree_1079') === false) {
		scrollToTop({
			appendTo : 'site',
			scrollTop : 'site',
			scrollTrigger : true,
			mobileSimple : true
		});
	};
	SliderInit({
		Type : 'fade',
		Anim : true
	});
	registerExtWindows('extwindow');
	recon.on('configureOptions', onConfigureOptions);
	recon.on('setFeatures', function(el) {
		if (el.dom.tagName.toLowerCase() === 'img' && el.dom.getAttribute('data-zoomsrc')) {
			el.dom.setAttribute('data-zoomsrc', el.dom.src.replace(/\/media\/med_/, '/medium/med_'));
			MojoZoom.init();
		}
	});
	var setQTips = function() {
		Ext.select('.variants').each(function(v) {
			if (!v.dom.firstChild) {
				v.remove();
				return;
			}
			v.setHeight(0);
			v.setVisible(false);
			var box = Ext.get(v.dom.firstChild).getBox(), w = box.width;
			if (w < 36) {
				w = 36
			}
			if (v.up('div').select('.ue_shop_overlay_window').getCount()) {
				var a = v.up('div').select('.ue_shop_overlay_window').first().down('a');
				a.dom.title = "";
				var img = a.select('img').first();
				/*img.dom.setAttribute('ext:qtip', v.dom.innerHTML);*/
				img.dom.setAttribute('ext:width', w);
			}
			var a2 = v.up('div').select('a').first();
			/*a2.dom.setAttribute('ext:qtip', v.dom.innerHTML);*/
			a2.dom.setAttribute('ext:width', w);
			a2.dom.title = "";
		});
		Ext.select('.variants').each(function(v) {
			v.remove();
		});
	};
	setQTips();
	recon.on('LiveGridHtmlReady', setQTips);
	recon.on('ShopfilterHtmlReady', setQTips);
	recon.on('ShopfilterHtmlReady', collectData.init('category', {
	   precall : function() {
        },
        recall   : function() {
	       collectData.integrate({
	           provider: 'kupona',
               type    : 'category',
               fields  : {
                kp_category_id  : 'tree'}});
            if(collectData.provider.hasOwnProperty('kupona')) {
                    setScript('kupona', 'https://d31bfnnwekbny6.cloudfront.net/customers/29054.min.js');
            }
        }}));
	Ext.QuickTips.enable();
	Ext.QuickTips.init();
	Ext.QuickTips.trackMouse = true;
	Ext.QuickTips.maxWidth = 600;
	if (Ext.get('extras') || Ext.get('tr_bannerbrands') || Ext.select('.categorydescription').first()) {
		LazyLoad.css([
			basepath + '__/styles/jquery.jscrollpane.css',
			basepath + '__/styles/jquery.jscrollpane.mondelli.css'
		]);
		loadJQueryPlugin([
			basepath + '__/scripts/jquery.mousewheel.js',
			basepath + '__/scripts/jquery.jscrollpane.js'
		], function() {
			$(function() {
				if ($(this).find('div.slider').length > 0) {
					$('.sfsection').each(function() {
						$(this).jScrollPane({
							showArrows : false,
							horizontalDragMaxWidth : 0
						});
						var api = $(this).data('jsp');
						var throttleTimeout;
						recon.on('ShopfilterHtmlReady', function() {
							var el = Ext.get(this);
							var box = el.select('.jspPane').first().getBox();
							var c = el.select('.jspContainer').first();
							c.setHeight(box.height);
							api.reinitialise();
						}.createDelegate(this))
						$(window).bind('resize', function() {
							if (!throttleTimeout) {
								throttleTimeout = setTimeout(function() {
									api.reinitialise();
									throttleTimeout = null;
								}, 50);
							}
						});
					})
				}
				if ($('#tr_bannerbrands div.desc').find('div.slider').length > 0) {
					$('#tr_bannerbrands div.desc').jScrollPane({
						showArrows : false,
						horizontalDragMaxWidth : 0
					});
					var api = $('#tr_bannerbrands div.desc').data('jsp');
					var throttleTimeout;
					recon.on('ShopfilterHtmlReady', function() {
						var el = Ext.select('tr_bannerbrands div.desc').first();
						var box = el.select('.jspPane').first().getBox();
						var c = el.select('.jspContainer').first();
						c.setHeight(box.height);
						api.reinitialise();
					}.createDelegate('#tr_bannerbrands div.desc'))
					$(window).bind('resize', function() {
						if (!throttleTimeout) {
							throttleTimeout = setTimeout(function() {
								api.reinitialise();
								throttleTimeout = null;
							}, 50);
						}
					});
				}
				if ($('.categorydescription div.faqdisplays').find('div.content').length > 0) {
					$('.categorydescription div.faqdisplays div.content').jScrollPane({
						showArrows : false,
						horizontalDragMaxWidth : 0
					});
					var api = $('.categorydescription div.faqdisplays div.content').data('jsp');
					var throttleTimeout;
					recon.on('ShopfilterHtmlReady', function() {
						var el = Ext.select('.categorydescription div.faqdisplays div.content').first();
						var box = el.select('.jspPane').first().getBox();
						var c = el.select('.jspContainer').first();
						c.setHeight(box.height);
						api.reinitialise();
					}.createDelegate('.categorydescription div.faqdisplays div.content'))
					$(window).bind('resize', function() {
						if (!throttleTimeout) {
							throttleTimeout = setTimeout(function() {
								api.reinitialise();
								throttleTimeout = null;
							}, 50);
						}
					});
				}
			});
		});
	}
	groessenlink();
    attrAutoSelect.init();
	copyInnerHTML({
		source : 'loginform_content',
		target : Ext.select('.ans_shop_detail_desc.left>.content').elements[0],
		smooth : true
	});
	registerMailWindow({
		linkClass : 'showyourfriend_trigger',
		contentID : 'content_mailform',
		successFunc : function(state) {
			if (state === true) {
				if (Ext.select('.ans_shop_by_look').first() != null) {
					Ext.select('#content_mailform h2.popup.title').first().dom.innerHTML = "Sie haben Ihren Lieblingslook entdeckt und m&ouml;chten ihn einem/r Freund/in zeigen?";
					Ext.select('#content_mailform h3.popup.intro').first().dom.innerHTML = "Nutzen Sie das folgende Formular, um Ihrem/r Freund/in eine pers&ouml;nliche Nachricht und den Link zu Ihrem gew&auml;hlten Look zu schicken:";
				};
			};
		},
		errorFunc : function(state) {
			if (state === true) {
				Ext.select('.syf_vis').each(function(el) {
					el.setVisible(false);
				});
			};
		}
	});
	dynMobileMenu({
		class : 'headm',
		serveLink : true,
		isTouch : isTouch()
	});
	spOnNoResult();
	Ext.fly(document.body).on('click', function(ev, el) {
		var spsc = el.id == 'spresult' ? Ext.get(el) : Ext.get(el).up('div#spresult'), spsco = Ext.get('spresult');
		if (!spsc && spsco) {
			spsco.hide();
		}
	});
});
runOnLoadFinish(function() {
	uniqueButton();
	slideShow({
		Wrapper : 'c_additional_wrapper',
		Elems : 'additionalpics',
		Navigation : true,
		NavigationOutside : true,
		Callback : function() {
			var addpics = Ext.select('.original img[rel^=additionalpics]');
			if (addpics.getCount() == 0) {
				Ext.select('.gt.ans_shop_media_link').setVisible(false);
			};
		}
	});
	slideShow({
		Wrapper : 'ue_intro_slider',
		Elems : 'ue_shop_look_intro',
		Navigation : true,
		NavigationOutside : false,
		StopOnMouseOver : true,
		Animation : 'default',
		Playable : true
	});
	unifyEls('shift_slider').each(function(el) {
		shiftSlider({
			cID : el.dom.id,
			initDelay : 5000,
			playDelay : 6000,
			autoPlay : true,
			autoPlaySlide : false,
			scrollAnim : {
				easing : 'easeOutStrong',
				duration : 1
			},
			navControl : true,
			pauseMOver : true
		});
	});
	setBoxRequest('openPopUp');
	tabActivate();
});
recon.on( 'beforeElevatezoom', function( zconfig, img ) {
    var src = img.dom.src.replace( /url\(["']?/, '' ).replace( /["']?\)/, '' ).replace( /\/media\/med_/, '/medium/med_' );
    img.dom.setAttribute( 'data-zoom-image', src );
    $( img.dom ).elevateZoom( zconfig );
    recon.on( 'setFeatures', function( el ) {
        if( el.dom.tagName.toLowerCase() === 'img' && el.dom.getAttribute( 'data-zoom-image' ) ) {
            var src = el.dom.src.replace( /\/media\/med_/, '/medium/med_' ),
                ez = $( el.dom ).data( 'elevateZoom' );
            el.dom.setAttribute( 'data-zoom-image', src );
            ez.swaptheimage( el.dom.src, src );
        }
    } );
    return (false);
} );