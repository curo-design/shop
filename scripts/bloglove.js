var commentExt	= {
	init	: function() {
		commentExt.def	= {
			cTrigger	: Ext.select('.interactive>.comment').first(),
			cList		: Ext.select('.commentslist').first(),
            cWrite      : Ext.get('commentwrite'),
			sHandle		: (document.body && document.body.hasOwnProperty('scrollTop')) ? document.body : document.documentElement
		};
		if(commentExt.def.cTrigger && commentExt.def.cList) {
			Ext.id(commentExt.def.cTrigger);
			Ext.id(commentExt.def.cList);
			commentExt.set(false);
		};
	},
	set		: function(config) {
		if(config == false) {
			config	= {
				cTrigger	: commentExt.def.cTrigger,
				cList		: commentExt.def.cList
			};
		};
		Ext.get(config.cTrigger).on('click', function() {
		  if(config.cList) {
		      if(commentExt.def.cWrite) {
		          commentExt.def.cWrite.setDisplayed(true);
            }
				Ext.fly(commentExt.def.sHandle).scrollTo('top', config.cList.getY()-200-commentExt.def.sHandle.scrollTop, true);
			};
		});
	}
};
function createPhotoElement(photo) {
	var innerHtml	= $('<img>').addClass('instagram-image').attr('src', photo.images.thumbnail.url);
	innerHtml		= $('<a>').attr('target', '_blank').attr('href', photo.link).append(innerHtml);
	return $('<div>').addClass('instagram-placeholder').attr('id', photo.id).append(innerHtml);
};
function extUser() {
	var d	= {
		user	: Ext.select('*[data-user]')
	};
	if(d.user.getCount() > 0) {
		d.user.removeAllListeners();
		d.user.on('click', function(ev, el) {
			var user	= el.getAttribute('data-user'),
			url			= document.location.pathname + '?',
			param		= recon.parseUrl(),
			urlparam	= [];
            param.user	= encodeURIComponent(user);
            foreach(param, function(v, k) { 
				urlparam.push(k +'=' + v);
            });
            document.location.href	= url + urlparam.join('&');
		});
	};
};
function extTags() {
	var d	= {
		tags	: Ext.select('*[data-tag]'),
		user	: Ext.select('*[data-user]')
	};
	if(d.tags.getCount() > 0) {
		d.tags.removeAllListeners();
		d.tags.on('click', function(ev, el) {
			var tag		= el.getAttribute('data-tag'),
			url			= basepath + 'r_3917' + '?',
			param		= recon.parseUrl(),
			urlparam	= [];
            param.tag	= encodeURIComponent(tag);
            foreach(param, function(v, k) { 
				urlparam.push(k +'=' + v);
            });
            document.location.href	= url + urlparam.join('&');
		});
	};
	if(d.user.getCount() > 0) {
		d.user.removeAllListeners();
		d.user.on('click', function(ev, el) {
			var user	= el.getAttribute('data-user'),
			url			= basepath + 'r_3917' + '?',
			param		= recon.parseUrl(),
			urlparam	= [];
            param.user	= encodeURIComponent(user);
            foreach(param, function(v, k) { 
				urlparam.push(k +'=' + v);
            });
            document.location.href	= url + urlparam.join('&');
		});
	};
};
runOnLoad(function() {
	extUser();
	commentExt.init();
	LazyLoad.js(
		basepath + '_/scripts/extensionsmin/jquery/jquery.js',
		function() {
			LazyLoad.js(
				basepath + '__/scripts/instagram.js',
				function() {
					jQuery(function($) {
						$('.rightblock_content.instagram').on('willLoadInstagram', function(event, options) {
							//console.log(options);
						});
						$('.rightblock_content.instagram').on('didLoadInstagram', function(event, response) {
							var that	= this;
							$.each(response.data, function(i, photo) {
								$(that).append(createPhotoElement(photo));
							});
							$(that).append('<div class="cleaner"></div>');
						});
						$('.rightblock_content.instagram').instagram({
							userId		: '353085067',
							accessToken	: '353085067.d91319c.d7e79cf978344a2e84bfeff1522dee76',
							clientId	: 'd91319c1d32f4e0e985fab2af69b4c55',
							count		: 9
						});
					});
			});
	});
});
runOnLoadFinish(function() {
	extTags.defer(500);
    (function() {
        var elClass	= ['miniature','lightbox'];
        var artClass= '.ue_shop_teaser.content img';
        Ext.select(artClass).each(function(el) {
            el.dom.outerHTML = '<a href="' + el.dom.src + '" class="' + elClass.join(' ') + '" title="' + el.dom.alt + '">' + el.dom.outerHTML + '</a>';
        });
        for(var int = 0; int < elClass.length; int++) {
            elClass[int] = '.' + elClass[int];
        }
        elClass = elClass.join('');
        if(!Ext.ux.Lightbox) {
            LazyLoad.js(basepath + '_/scripts/extensions/lightbox.js', function() {
                registerLB('a' + elClass);
            });
       } else {
                registerLB('a' + elClass);
      };
   }).defer(500);
    function registerLB(elClass) {
            Ext.ux.Lightbox.register(elClass, true );
   };
});