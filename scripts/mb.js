function scrollToView(config) {
	var def = {
		trigger		:	config.trigger		||	'scrollTrigger',
		scrollEl	:	config.scrollEl		||	'scrollElement',
		marginTop	:	config.marginTop	||	0
	};
	if(Ext.get(def.trigger) != null && Ext.get(def.scrollEl) != null) {
		Ext.get(def.trigger).on('click', function() {
			Ext.get(def.scrollEl).scrollTo('top', window.document.documentElement.clientHeight + def.marginTop, true);
		}, undefined, {
			preventDefault	:	true
		});
	};
};
function setOrientation(config) {
	var def = {
		Elements	:	config.Elements		|| false,
		Landscape	:	config.Landscape	|| false,
		Portrait	:	config.Portrait		|| false,
		Container	:	config.Container	|| false,
		cElement	:	config.cElement		|| false
	};
	if(def.Elements !== false && def.Landscape !== false && def.Portrait !== false && def.Container !== false && def.Element !== false) {
		var styleObj	=	{
			tag			:	'style',
			type		:	'text/css',
			html		:	''
		};
		for(var int = 0; int < def.Elements.length; int++) {
			styleObj.html += def.Elements[int] + ((int == def.Elements.length - 1) ? ' {' : ',') + '\n';
		};
		// Element ist breiter aber nicht h�her als Container, mach Portrait
		if((def.cElement.offsetWidth >= def.Container.offsetWidth) && (def.cElement.offsetHeight < def.Container.offsetHeight)) {
			for(var int = 0; int < def.Portrait.length; int++) {
				styleObj.html += '\t' + def.Portrait[int] + '\n';
			};
		// Element ist h�her aber nicht breiter als Container, mach Landscape
		} else if((def.cElement.offsetWidth < def.Container.offsetWidth) && (def.cElement.offsetHeight >= def.Container.offsetHeight)) {
			for(var int = 0; int < def.Landscape.length; int++) {
				styleObj.html += '\t' + def.Landscape[int] + '\n';
			};
		};
		styleObj.html += '}';
		Ext.get(document.body).createChild(styleObj);
	};
};
function setVideo(config) {
	var def = {
		videoID	:	config.videoID	||	'videoplay',
		controls:	config.controls	||	'controlsvideo',
		buttons	:	config.buttons	|| {
			playpause	:	'playpause',
			mute		:	'mutebutton'
		},
		autoplay	:	config.autoplay		|| false
	};
	var diffAgents = [
		'iPad',
		'iPhone'
	];
	if(Ext.get(def.videoID) != null && Ext.get(def.controls) != null && Ext.get(def.buttons.playpause) != null && Ext.get(def.buttons.mute) != null) {
		var movie		=	Ext.get(def.videoID);
		var mute		=	Ext.get(def.buttons.mute);
		var controls	=	Ext.get(def.controls);
		var playpause	=	Ext.get(def.buttons.playpause);
		var buttons		=	{
			play	:	'<img src="/__/video/play2.png" alt="" title="play" />',
			pause	:	'<img src="/__/video/pause.png" alt="" title="pause" />',
			son		:	'<img src="/__/video/soundon.png" alt="" title="sound on" />',
			soff	:	'<img src="/__/video/soundoff.png" alt="" title="sound off" />'
		};
		movie.setStyle({
			zIndex	:	2
		});
		(def.autoplay === true) ? movie.dom.play() : movie.dom.pause();
		playpause.dom.innerHTML = (movie.dom.paused === false) ? buttons.pause : buttons.play;
		mute.on('click', function () {
			if(movie.dom.muted === true) {
				movie.dom.muted = false;
				mute.dom.innerHTML = buttons.soff;
			} else {
				movie.dom.muted = true;
				mute.dom.innerHTML = buttons.son;
			};
		}, undefined, {
			preventDefault	:	true
		});
		playpause.on('click', function () {
			if(movie.dom.paused === true) {
				movie.dom.play();
				playpause.dom.innerHTML = buttons.pause;
			} else {
				movie.dom.pause();
				playpause.dom.innerHTML = buttons.play;
			};
		}, undefined, {
			preventDefault	:	true
		});
		for(var int = 0; int < diffAgents.length; int++) {
			if(navigator.userAgent.indexOf(diffAgents[int])!=-1) {
				switch(diffAgents[int]) {
					case 'iPad':
						mute.setStyle({
							opacity	:	'0.2'
						});
						playpause.dom.innerHTML = buttons.play;
					break;
					case 'iPhone':
						controls.setStyle({
							visibility	:	'hidden'
						});
					break;
					default:
					break;
				};
			};
		};
	};
};
// Eigener EventListener
function AttributeListener(obj) {
	var targ = Ext.get('site_article');
	var elems = Ext.select('#slider>a');
	if(obj != null && targ != null) {
		if(obj.getStyle('display') == 'block') {
			targ.setVisible(true, true).setOpacity(1, true);
		} else {
			targ.setVisible(false, true).setOpacity(0, true);
		};
	};
};
runOnLoad(function() {
	setOrientation({
		Elements	: [
			// Slider-Elemente // Start
			'.nivo-imageLink img',
			'.nivo-main-image',
			'.nivo-slice img',
			// Slider-Elemente // Ende
			'#site_image img',
			'.backstretch .nivoSlider a.nivo-imageLink img',
			'.backstretch .nivoSlider .nivo-main-image',
			'.backstretch .nivoSlider .nivo-slice img',
		],
		cElement	: ((Ext.select('.nivo-imageLink img').first() != null) ? Ext.select('.nivo-imageLink img').first().dom : Ext.select('#site_image img').first().dom),
		Landscape	: ['width:100% !important;','height:auto !important;'],
		Portrait	: ['width:auto !important;','height:100% !important;'],
		Container	:	Ext.get('backstretch').dom
	});
	Ext.select('body').first().dom.onresize = function() {
		setOrientation({
			Elements	: [
				// Slider-Elemente // Start
				'.nivo-imageLink img',
				'.nivo-main-image',
				'.nivo-slice img',
				// Slider-Elemente // Ende
				'#site_image img',
				'.backstretch .nivoSlider a.nivo-imageLink img',
				'.backstretch .nivoSlider .nivo-main-image',
				'.backstretch .nivoSlider .nivo-slice img',
			],
			cElement	: ((Ext.select('.nivo-imageLink img').first() != null) ? Ext.select('.nivo-imageLink img').first().dom : Ext.select('#site_image img').first().dom),
			Landscape	: ['width:100% !important;','height:auto !important;'],
			Portrait	: ['width:auto !important;','height:100% !important;'],
			Container	:	Ext.get('backstretch').dom
		});
	};
	scrollToView({
		trigger		:	'site_goto_link',
		scrollEl	:	(Ext.isChrome === true || Ext.isSafari === true) ? window.document.body : window.document.documentElement,
		marginTop	:	-151
	});
	setVideo({
		videoID	:	'videoplay',
		controls:	'controlsvideo',
		buttons	:	{
			playpause	:	'playpause',
			mute		:	'mutebutton'
		},
		autoplay		: true
	});
});
    runOnLoadFinish(function() {
        Ext.select('.mytooltip').each(function( el ){
            el.up('div').on('mouseover',function(){
                this.select('.tooltipthumb img[img-data-url]').each(function(imgel) {
                    var uri = imgel.dom.getAttribute('img-data-url');
                    if (uri != '' && imgel.dom.src != uri) {
                        imgel.dom.src = uri; 
                    };
                }); 
            }); 
        });
    });