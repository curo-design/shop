// Achims Slider "Hurra Hurra, wie wunderbar"
function SliderInit(Slider) {
    slide = {
        Type : Slider.Type || 'slide', // slide, fade
        Container : Ext.get(Slider.Container || 'mySlider_container'),
        Control_l : Ext.get(Slider.Control_l || 'mySlider_control_l'),
        Control_r : Ext.get(Slider.Control_r || 'mySlider_control_r'),
        Content : Ext.get(Slider.Content || 'mySlider_content'),
        ToSlide : Ext.get(Slider.ToSlide || 'mySlider_toslide'),
        Elements : Ext.select('.' + (Slider.Element || 'mySlider_element')),
        Anim : Slider.Anim || false
    };
    if (isNull(slide.Container) || isNull(slide.Control_l) || isNull(slide.Control_r) || isNull(slide.Content) || isNull(slide.ToSlide) || isNull(slide.Elements.elements[0])) {
        return;
    };
    (slide.Anim === true) ? slide.Anim = {
        easing : 'easeOut',
        duration : .5,
        delay : 0
    } : slide.Anim;
    slide.Content.setPositioning({
        position : 'relative'
    });
    if (slide.Type == 'fade') {
        slide.ToSlide.setPositioning({
            left : '0px',
            top : '0px',
            position : 'relative'
        });
        slide.ToSlide.setWidth(Ext.get(slide.Elements.elements[0]).getWidth());
        slide.ToSlide.setHeight(Ext.get(slide.Elements.elements[0]).getHeight());
        slide.Elements.setPositioning({
            left : '0px',
            top : '0px',
            position : 'absolute'
        });
        slide.Elements.setStyle({
            opacity : 0,
            zIndex : 5
        });
        Ext.get(slide.Elements.elements[0]).setStyle({
            opacity : 1,
            zIndex : 10
        });
    } else if (slide.Type == 'slide') {
        slide.ToSlide.setPositioning({
            left : '0px',
            top : '0px',
            position : 'absolute'
        });
        slide.Elements.Width = Ext.get(slide.Elements.elements[0]).getWidth();
        slide.Elements.Margins = Ext.get(slide.Elements.elements[0]).getMargins();
        slide.Elements.MaxWidth = slide.Elements.Width + slide.Elements.Margins.right;
        slide.Content.MaxWidth = slide.Content.getWidth();
    };
    slide.Counter = 0;
    slide.Control_l.on('click', slideTo, slide.Control_l, slide);
    slide.Control_r.on('click', slideTo, slide.Control_r, slide);
};
function slideTo(Event, Function, Parameter) {
    Parameter.Direction = (this.dom.id == Parameter.Control_l.dom.id || this.dom.id == Parameter.Control_r.dom.id) ? true : false;
    if (Parameter.Direction == false) {
        return;
    };
    if (Parameter.Type == 'slide') {
        if (this.dom.id == Parameter.Control_l.dom.id && Parameter.Counter > 0) {
            Parameter.ToSlide.moveTo(Parameter.ToSlide.getX() + slide.Elements.MaxWidth, Parameter.ToSlide.getY(), Parameter.Anim);
            Parameter.Counter--;
        } else if (this.dom.id == Parameter.Control_r.dom.id && Parameter.Counter < (Parameter.Content.MaxWidth / Parameter.Elements.MaxWidth)) {
            Parameter.ToSlide.moveTo(Parameter.ToSlide.getX() - slide.Elements.MaxWidth, Parameter.ToSlide.getY(), Parameter.Anim);
            Parameter.Counter++;
        };
    } else if (Parameter.Type == 'fade') {
        Parameter.Elements.setStyle({
            zIndex : 5
        });// Nach rechts: counter zIndex gr��er als folgender, danach tauschen
        if (this.dom.id == Parameter.Control_l.dom.id && Parameter.Counter > 0) {
            if (Parameter.Anim !== false && !Parameter.Anim.callback) {
                Parameter.Anim.callback = function(el) {
                    el.setStyle({
                        zIndex : 5
                    });
                    Ext.get(Parameter.Elements.elements[Parameter.Counter]).setStyle({
                        zIndex : 10
                    })
                };
            };
            Ext.get(Parameter.Elements.elements[Parameter.Counter]).setStyle({
                zIndex : 10
            }).setOpacity(0, Parameter.Anim);
            Parameter.Counter--;
        } else if (this.dom.id == Parameter.Control_r.dom.id && Parameter.Counter < Parameter.Elements.elements.length - 1) {
            if (Parameter.Anim !== false && !Parameter.Anim.callback) {
                Parameter.Anim.callback = function(el) {
                    el.setStyle({
                        zIndex : 5
                    });
                    Ext.get(Parameter.Elements.elements[Parameter.Counter]).setStyle({
                        zIndex : 10
                    })
                };
            };
            Ext.get(Parameter.Elements.elements[Parameter.Counter]).setStyle({
                zIndex : 10
            }).setOpacity(0, Parameter.Anim);
            Parameter.Counter++;
        };
        Ext.get(Parameter.Elements.elements[Parameter.Counter]).setOpacity(1);
    };
};
//Detailbild tauschen
function SwapDetailPic(Handler) {
	var	container = Ext.select('#ans_shop_media_detail img').first();
	container.dom.src = Handler.href;
	var src = Handler.href.replace( /url\(["']?/, '' ).replace( /["']?\)/, '' ).replace( /\/media\/med_/, '/org/med_' );
	container.dom.setAttribute('data-zoomsrc', src);
	MojoZoom.init();
};

/** ************************************** */
var extendLogin = function(lnk) {
    var box = Ext.get('kopflogin_data');
    var appendbox = Ext.get('lh_login');
    if (box) {
        var mode = box.isDisplayed() == false || box.isDisplayed() == 'none' ? 'block' : 'none';
        box.setDisplayed(mode);
        checkLoginBox(lnk);
    }
};
/** * Loginbox ein/ ausblenden / steuern ** */
var checkLoginBox = function(lnk) {
    var box = Ext.get('kopflogin_data');
    var lnktoappend = lnk || 'lh_login';
    var appendbox = Ext.get(lnktoappend);
    if (box) {
        if (box.isDisplayed() == true) {
            var posx = appendbox.getX() - (box.getWidth() / 2 + 20);
            var posy = appendbox.getY() + appendbox.getHeight();
            var username = box.select('input[name=username]');
            if (username && username.elements && username.elements[0]) {
                Ext.get(username.elements[0]).dom.focus();
            }
            box.setX(posx);
            box.setY(posy);
        }
    }
};
function uniqueButton(bClass) {
	Ext.select('#site_article .cs3button').each(function(el) {
		el.replaceClass('cs3button', 'mgbutton');
		el.setStyle({
			fontSize: '12px',
			width: 'auto'
		});
	});
};
var registerExtWindows = function(classname, lybcall) {
	var links = Ext.select('a[class^=' + classname + '] ', true);
	var lyb = (lybcall && lybcall != false && lybacll != '') ? lybcall	: '674e8089ce17730071c984baf55689da';
	if (links) {
		links.each(function(el) {
					var lnk = el.dom;
					lnk.onclick = function() {
						var box = Ext.MessageBox.show({
									title : '',
									msg : '<img src=\"'+ basepath+ '_/scripts/ext/resources/images/default/grid/loading.gif\" alt=\"\" style=\"border:0; vertical-align:middle;\"> Loading...',
									animEl : el,
									width : 616,
									height : 610,
									modal : true,
									fn : function() {
										Ext.MessageBox.hide();
									}
								});
						Ext.Msg.getDialog().body.addClass('printdiv');
						var lnkopt = lnk.href.split('?');
						var url = '';
						if (lnkopt[1] && lnkopt[1] != '') {
							url = lnk.href + '&_lyb=' + lyb;
						} else {
							url = lnk.href + '?_lyb=' + lyb;
						};
						Ext.Ajax.request({
									url : url,
									success : function(req) {
										var txt = decode(req.responseText);
										var dlg = box.getDialog();
										box.updateText('<div style=\"width:576px; height:560px; overflow:auto;\">'+ txt.html + '</div>');
										dlg.resizeTo(616, 610);
									}
								});
						return (false);
				};
		});
	};
};
/* Facebook Share Button */
function fbShare() {
	window.open(
		'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 
		'facebook-share-dialog', 
		'width=626,height=436'
	); 
};
//Haut den Wunschzettel-Titel in die besagten Elemente wenn Seite dem Wunschzettel entspricht
function extWunschzettel() {
	var conf = {
		triggerEl: '.bookmark_site',
		headEl: '#site_article_head h1 a',
		footEl: '#site_breadcrumb_foot span a span',
		replaceText: 'Wunschzettel'
	};
	if(typeof(Ext.select(conf.triggerEl).elements[0]) != 'object') {
		return;
	};
	if(typeof(Ext.select(conf.headEl).elements[0]) == 'object') {
		Ext.select(conf.headEl).elements[0].innerHTML = conf.replaceText;
	};
	if(typeof(Ext.select(conf.footEl).elements[0]) == 'object') {
		Ext.select(conf.footEl).elements[Ext.select(conf.footEl).elements.length - 1].innerHTML = conf.replaceText;
	};
};
runOnLoad(function() {
    SliderInit({
        Type : 'fade',
        Anim : true
    });
    var zoomables = false, zoomContainer;
    Ext.select( 'img.zoomImg' ).each( (function( img ) {
        var src = img.dom.src.replace( /url\(["']?/, '' ).replace( /["']?\)/, '' ).replace( /\/media\/med_/, '/org/med_' );
        img.dom.setAttribute( 'data-zoomsrc', src );
        zoomables = true;
        if( !zoomContainer ) {
            var details = Ext.select( '.ans_shop_detail' ).first();
            details.dom.style.position = 'relative';
            zoomContainer = details.createChild( {tag: 'div', id: 'productImage_zoom', style: 'position:absolute;'} );
            zoomContainer.setBox(details.getBox());
        }
    }) );
    if( zoomables ) {
        LazyLoad.css( basepath + '__/scripts/mojozoom.css' );
        LazyLoad.js( basepath + '__/scripts/mojozoom.js', function() {
            MojoZoom.init();
        } );
    };
	registerExtWindows('extwindow');
});
runOnLoadFinish(function() {
	uniqueButton('mgbutton');
	extWunschzettel();
});