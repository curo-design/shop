recon.modules.advent = {
    tab: null,
    adv: null,
    advdata: null,
    advid: null,
    advcnt: null,
    advIsSet: false,
    solution: '',
    sdata: null,
    odata: null,
    init: function() {
        this.tab = recon.tabs.getSelectedTab();
        this.advid = this.tab.contentID;
        this.adv = Ext.get( 'advbg' + this.advid );
        this.advcnt = Ext.get( 'adv' + this.advid );
        if( this.advIsSet ) {
            this.advdata = this.adv.getBox();
            this.makeDraggable();
        }
        var solution = Ext.select( 'input[name^=adv_solution]' );
        var sfield = solution.first();
        if( sfield ) {
            this.solution = sfield.dom.value;
            sfield.on( 'change', function() {
                if( this.dom.value.split( ' ' ).join( '' ).length < 24 ) {
                    recon.window.showDialog( 'Fehler', 'Der L&ouml;sungssatz mu&szlig; mindestens 24 Zeichen enthalten.' );
                } else {
                    var s = this.dom.value.replace( /[^a-z0-9äöüß]/gi, '_' );
                    var nums = [];
                    var info = [];
                    for( var i = 0; i < s.length; i++ ) {
                        if( s.substr( i, 1 ) != '_' ) {
                            nums.push( i );
                        }
                    }
                    if( nums.length > 0 ) {
                        nums.shuffle();
                        nums = nums.slice( 0, 24 );
                    }
                    i = 0;
                    info = [];
                    nums.each( function( pos ) {
                        info[i++] = pos;
                    } );
                    var sdiv = Ext.getDom( 'solution' + recon.modules.advent.advid );
                    if( sdiv ) {
                        sdiv.innerHTML = '';
                        var z = 1;
                        info.each( function( b ) {
                            Ext.DomHelper.append( sdiv, {
                                tag: 'span',
                                style: 'border: 1px solid #000000; margin: 1px; padding: 2px;',
                                html: '<input type="hidden" name="adv_options[solution][' + z + ']" value="' + b + '">' + z + ': <b>' + s.substr( b, 1 ) + '</b>'
                            } );
                            z++;
                        } );
                    }
                }
            } );
        }
        if( this.sdata ) {
            this.onMediaSelect( this.sdata, 'adv_symbolclosed' );
        }
        if( this.odata ) {
            this.onMediaSelect( this.odata, 'adv_symbolopened' );
        }
    },
    onMediaSelect: function( data, tgtid ) {
        var elid = tgtid || false;
        if( elid ) {
            var fld = elid.split( '_' )[1], src, z, el;
            switch( fld.substr( 0, 12 ) ) {
                case 'symbolopened' :
                    src = data.url;
                    for( z = 1; z <= 24; z++ ) {
                        el = Ext.get( 'link_' + z );
                        if( el ) {
                            el.on( 'mouseover', function() {
                                this.dom.style.backgroundImage = 'url(' + src + ')';
                            } );
                            el.on( 'mouseout', function() {
                                var ourl = recon.modules.advent.sdata.url || '';
                                this.dom.style.backgroundImage = 'url(' + ourl + ')';
                            } );
                        }
                    }
                    break;
                case 'symbolclosed' :
                    var advbg = Ext.get( 'advbg' + this.advid );
                    if( advbg && this.advdata ) {
                        src = data.url;
                        var padding = 10;
                        var imgwidth = Number( data.size[0] );
                        var imgheight = Number( data.size[1] );
                        var row = 0;
                        var col = 0;
                        for( z = 1; z <= 24; z++ ) {
                            el = Ext.get( 'link_' + z );
                            if( el ) {
                                el.dom.style.lineHeight = imgheight + 'px';
                                el.dom.style.backgroundImage = 'url(' + src + ')';
                                el.dom.style.width = imgwidth + 'px';
                                el.dom.style.height = imgheight + 'px';
                                recon.modules.advent.sdata.url = src;
                            } else {
                                var cx = (imgwidth * col) + (padding * (col + 1));
                                if( cx + imgwidth > this.advdata.width ) {
                                    row++;
                                    col = 0;
                                    cx = padding;
                                }
                                var cy = (row * imgheight) + (padding * (row + 1));
                                var fx = Ext.getDom( 'options_link_posx' + z );
                                if( fx && fx.value != '' ) {
                                    cx = fx.value;
                                } else if( fx ) {
                                    fx.value = cx;
                                }
                                var fy = Ext.getDom( 'options_link_posy' + z );
                                if( fy && fy.value != '' ) {
                                    cy = fy.value;
                                } else if( fy ) {
                                    fy.value = cy;
                                }
                                /*
                                 TODO: Da mit der Höhne nochmal überdenken...
                                 */
                                var txtel = Ext.DomHelper.append( advbg, {
                                    tag: "span",
                                    html: '<h1 style="font-size:' + (imgheight - 10) + 'px;">' + z + '</h1>',
                                    id: 'link_' + z,
                                    style: 'display:block; text-align:center; line-height:' + imgheight + 'px; background:url(' + src + ') top left no-repeat; width:' + imgwidth + 'px; height:' + imgheight + 'px; top:' + cy + 'px; left:' + cx + 'px;'
                                }, true );
                                txtel.setOpacity( .75 );
                                //txtel.advnum = z;
                                col++;
                            }
                        }
                        this.makeDraggable();
                    }
                    break;
                case 'bgpic' :
                    if( this.adv && data.mediafile ) {
                        this.adv.dom.style.background = "url(" + data.url + ")";
                        this.adv.dom.style.width = data.width + "px";
                        this.adv.dom.style.height = data.height + "px";
                        this.advdata = this.adv.getBox();
                        if( !this.advIsSet ) {
                            this.advIsSet = true;
                        }
                    }
                    break;
            }
        }
    },
    makeDraggable: function() {
        if( this.adv ) {
            this.adv.initDDTarget( 'adv' + this.advid );
            var elements = this.adv.select( "span" );
            elements.each( this.prepareLink, this );
        }
    },
    prepareLink: function( el ) {
        var dd = el.initDD( 'adv' + this.advid );
        dd.startDrag = function() {
            this.constrainTo( recon.modules.advent.adv );
        };
        dd.endDrag = function() {
            var data = this.id.split( '_' );
            var id = data[1];
            recon.modules.advent.advdata = recon.modules.advent.adv.getBox();
            var x = this.lastPageX - recon.modules.advent.advdata.x;
            var y = this.lastPageY - recon.modules.advent.advdata.y;
            var fx = Ext.getDom( 'options_link_posx' + id );
            if( fx ) {
                fx.value = x;
            }
            var fy = Ext.getDom( 'options_link_posy' + id );
            if( fy ) {
                fy.value = y;
            }
        };
        el.dom.style.cursor = "move";
    },
    updateLink: function() {
    }
};
var advResultBtn = function( aid, n ) {
    new Ext.Button( 'options_link' + n + '_showdata', {
        text: 'Tagesgewinner',
        day: n,
        handler: function( btn ) {
            var url = recon.filename + '?_mod=advent&_func=showWinners&_id=' + aid + '&_day=' + btn.day;
            recon.ajax.request( {
                url: url,
                success: function( req ) {
                    recon.window.showDialog( 'Gewinner', req.responseText, 'ok', 800 );
                }
            } );
        }
    } );
};