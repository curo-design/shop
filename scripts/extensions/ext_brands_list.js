function getBrandTrigger(id) {
	var el	= Ext.get(id);
	var elc	= '.mytooltip';
	el.on('mouseover',function() {
		if(el.child(elc) && el.child(elc).dom.innerHTML == '') {
			el.child(elc).load('?_func=getBrand', {
				brandID		: el.child(elc).dom.dataset.brand,
				brandLabel	: el.child(elc).dom.dataset.label,
				tpl			: el.child(elc).dom.dataset.tpl
			});
		};
	}); 
};

runOnLoadFinish(function(){
	Ext.select('.tooltipcontainer').each(function(el) {
		Ext.id(el);
		getBrandTrigger(el.dom.id);
	});
});