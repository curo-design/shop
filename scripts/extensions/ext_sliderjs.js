// Slider bei Whats new
function slideShowExt(conf) {
	var def = {
		Wrapper				: Ext.get(conf.Wrapper			|| 'wrap_slider'),
		Elems				: Ext.select('.' + (conf.Elems	|| 'slide_elements')),
		ElemsAll			: conf.ElemsAll					|| {},
		Navigation			: conf.Navigation				|| false,
		NavigationOutside	: conf.NavigationOutside		|| false,
		Playable			: conf.Playable					|| false,
		StopOnMouseOver		: conf.StopOnMouseOver			|| false,
		Duration			: conf.Duration					|| 500,
		Delay				: conf.Delay					|| 3000,
		Type				: conf.Type						|| 'left',
		Callback			: conf.Callback					|| false,
		pointEvent			: conf.pointEvent				|| 'click'
	};
	if (!def.Wrapper && (def.Callback !== false && (typeof def.Callback) == 'function')) {
		Ext.callback(def.Callback, this, def);
		return;
	} else if (!def.Wrapper && (def.Callback === false || (typeof def.Callback) != 'function')) {
		return;
	};
	def.Elems.ElemCounter = {
		active : 0,
		once : def.Elems.elements.length,
		twice : def.Elems.elements.length * 2
	};
	// Standard-Animation
	switch (conf.Animation) {
		case true :
			def.Animation = true;
			break;
		case 'default' :
			def.Animation = {
				duration : def.Duration / 1000,
				delay : 0
			};
			break;
		default :
			def.Animation = false;
			break;
	}
	if (def.Wrapper === null) {
		return;
	}
	if (def.Wrapper.dom.parentElement.id == '') {
		def.Wrapper.dom.parentElement.id = def.Wrapper.dom.id + '_slider_container';
	};
	def.Container	= Ext.get(def.Wrapper.dom.parentElement.id);
	// Mehr Inhalt als anzeigbar?
	def.Elems.Width = 0;
	def.Elems.each(function(el) {
		def.Elems.Width += el.getWidth() + el.getMargins().left + el.getMargins().right;
	});
	// Inhalt passt in Anzeigebereich
	if (def.Container.getWidth() >= def.Elems.Width) {
		def.Navigation = false;
		def.Playable = false;
		def.Elems.each(function(el) {
			if (typeof (el.child('img')) != 'undefined') {
				el.addClass('original');
			};
		});
		// Inhalt passt nicht in Anzeigebereich
	} else {
		// Breite des Wrappers setzen
		def.Elems.each(function(el) {
			Ext.DomHelper.append(def.Wrapper, el.dom.outerHTML);
			if (typeof (el.child('img')) != 'undefined') {
				el.addClass('original');
			};
		});
		def.ElemsAll = Ext.select('.' + (conf.Elems || 'slide_elements'));
		def.ElemsAll.Width = def.Elems.Width * 2;
		def.Wrapper.setWidth(def.ElemsAll.Width, false);
	}
	if(Ext.select('html.touch').first()) {
		def.pointEvent	= 'touchstart';
	};
	def.initWidth	= window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	// Initial-Position des Wrappers setzen
	def.Wrapper.defX	= def.Wrapper.getX();
	def.Wrapper.defMaxX	= def.Wrapper.getX() - def.Elems.Width;
	def.Wrapper.currX	= def.Wrapper.defX;
	def.Wrapper.setX(def.Wrapper.defX, false);
	// Hat der Slider eine Navigation? --> Initiieren
	if (def.Navigation === true) {
		var nav = {
			class : def.Container.dom.id + '_slider_nav',
			id : def.Container.dom.id + '_sln_',
			tpl : [
				'<a href="#" on' + def.pointEvent + '="return (false);" rel="nofollow" class="',
				'" id="',
				'"></a>'
			],
			type : [
				'left',
				'right'
			]
		};
		// Ist die Navigation im Root-Element oder nicht?
		if (def.NavigationOutside === false) {
			Ext.DomHelper.append(def.Container, (nav.tpl[0] + nav.class + nav.tpl[1] + nav.id + nav.type[0] + nav.tpl[2]));
			Ext.DomHelper.append(def.Container, (nav.tpl[0] + nav.class + nav.tpl[1] + nav.id + nav.type[1] + nav.tpl[2]));
		} else if (def.NavigationOutside === true) {
			if (def.Container.dom.parentElement.id == '') {
				def.Container.dom.parentElement.id = 'parent_' + def.Container.dom.id;
			};
			def.pContainer = Ext.get(def.Container.dom.parentElement.id);
			def.pContainer.setPositioning({
				position : 'relative'
			});
			Ext.DomHelper.append(def.pContainer, (nav.tpl[0] + nav.class + nav.tpl[1] + nav.id + nav.type[0] + nav.tpl[2]));
			Ext.DomHelper.append(def.pContainer, (nav.tpl[0] + nav.class + nav.tpl[1] + nav.id + nav.type[1] + nav.tpl[2]));
		};

		def.Nav = Ext.select('.' + nav.class);
		def.Nav.each(function(el) {
			if (el.dom.id === (nav.id + nav.type[0])) {
				el.on(def.pointEvent, function() {
					def.Type = 'right';
					slideShowExtTo(def);
				});
			} else if (el.dom.id === (nav.id + nav.type[1])) {
				el.on(def.pointEvent, function() {
					def.Type = 'left';
					slideShowExtTo(def);
				});
			};
		});
	};
	// Ist der Slider abspielbar?
	if (def.Playable === true) {
		var slideInit = window.setInterval(function() {
			slideShowExtTo(def);
		}, def.Delay);
		if (def.StopOnMouseOver === true) {
			def.ElemsAll.each(function(el) {
				el.on('mouseover', function() {
					window.clearInterval(slideInit);
				});
				el.on('mouseout', function() {
					slideInit = window.setInterval(function() {
						slideShowExtTo(def);
					}, def.Delay);
				});
			});
			def.Nav.each(function(el) {
				el.on('mouseover', function() {
					window.clearInterval(slideInit);
				});
				el.on('mouseout', function() {
					slideInit = window.setInterval(function() {
						slideShowExtTo(def);
					}, def.Delay);
				});
			});
		}
	}
	if (def.Callback !== false && (typeof def.Callback) == 'function') {
		Ext.callback(def.Callback, this, def);
	};
	var isResizing	= false;
	window.onresize	= function() {
		if(isResizing === false) {
			isResizing		= true;
			window.setTimeout(function() {
				recalcSlider(def);
			}, 500);
		};
	};
	function recalcSlider(conf) {
		var actWidth	= window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		var nElemsWidth	= 0;
		def.Elems.each(function (el) {
			nElemsWidth	+= el.getWidth() + el.getMargins().left + el.getMargins().right;
		});
		if(def.initWidth != actWidth && def.Elems.Width != nElemsWidth) {
			def.Elems.Width		= nElemsWidth;
			def.Elem.Width		= def.Elems.Width / def.Elems.elements.length;
			def.ElemsAll.Width	= def.Elems.Width * 2;
			def.Wrapper.setWidth(def.ElemsAll.Width, false);
			def.Wrapper.defX	= def.Container.getX();
			def.Wrapper.defMaxX	= def.Wrapper.getX() - def.Elems.Width;
			def.Wrapper.setX(def.Wrapper.defX - (def.Elem.Width * (def.Elems.ElemCounter.active)), false);
		};
		def.initWidth	= actWidth;
		isResizing		= false;
	};
	def.pContainer.setVisible(true, true).setOpacity(1, true);
	def.Container.setVisible(true, true).setOpacity(1, true);
	def.Wrapper.setVisible(true, true).setOpacity(1, true);
};
// Slide-Funktion
function slideShowExtTo(conf) {
	var def			= conf;
	def.Elem		= Ext.get(def.ElemsAll.elements[def.Elems.ElemCounter.active]);
	def.Elem.Width	= def.Elem.getWidth() + def.Elem.getMargins().left + def.Elem.getMargins().right; // Gesamtbreite
	// aktuelles Element
	// Slider nach links
	if (def.Type === 'left') {
		if (def.Elems.ElemCounter.active === def.Elems.ElemCounter.once) {
			def.Elems.ElemCounter.active = 0;
			def.Wrapper.setX(def.Wrapper.defX, false);
		}
		def.Wrapper.currX		= def.Wrapper.getX() - def.Elem.Width;
		def.Wrapper.setX(def.Wrapper.currX, def.Animation);
		def.Elems.ElemCounter.active++;
		// Slider nach rechts
	} else if (def.Type === 'right') {
		if (def.Elems.ElemCounter.active === 0) {
			def.Elems.ElemCounter.active = def.Elems.ElemCounter.once;
			def.Wrapper.setX(def.Wrapper.defMaxX, false);
		}
		def.Wrapper.currX	= def.Wrapper.getX() + def.Elem.Width;
		def.Wrapper.setX(def.Wrapper.currX, def.Animation);
		def.Elems.ElemCounter.active--;
	}
};