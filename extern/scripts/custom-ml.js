var farben = new Array();
String.prototype.htmlEntities = function()
	{
	  var chars = new Array ('&','�','�','�','�','�','�','�','�','�','�',
	                         '�','�','�','�','�','�','�','�','�','�','�',
	                         '�','�','�','�','�','�','�','�','�','�','�',
	                         '�','�','�','�','�','�','�','�','�','�','�',
	                         '�','�','�','�','�','�','�','�','�','�','�',
	                         '�','�','�','�','�','�','�','�','"','�','<',
	                         '>','�','�','�','�','�','�','�','�','�','�',
	                         '�','�','�','�','�','�','�','�','�','�','�',
	                         '�','�','�','�','�','�','�','�');

	  var entities = new Array ('amp','agrave','aacute','acirc','atilde','auml','aring',
	                            'aelig','ccedil','egrave','eacute','ecirc','euml','igrave',
	                            'iacute','icirc','iuml','eth','ntilde','ograve','oacute',
	                            'ocirc','otilde','ouml','oslash','ugrave','uacute','ucirc',
	                            'uuml','yacute','thorn','yuml','Agrave','Aacute','Acirc',
	                            'Atilde','Auml','Aring','AElig','Ccedil','Egrave','Eacute',
	                            'Ecirc','Euml','Igrave','Iacute','Icirc','Iuml','ETH','Ntilde',
	                            'Ograve','Oacute','Ocirc','Otilde','Ouml','Oslash','Ugrave',
	                            'Uacute','Ucirc','Uuml','Yacute','THORN','euro','quot','szlig',
	                            'lt','gt','cent','pound','curren','yen','brvbar','sect','uml',
	                            'copy','ordf','laquo','not','shy','reg','macr','deg','plusmn',
	                            'sup2','sup3','acute','micro','para','middot','cedil','sup1',
	                            'ordm','raquo','frac14','frac12','frac34');

	  newString = this;
	  for (var i = 0; i < chars.length; i++)
	  {
	    myRegExp = new RegExp();
	    myRegExp.compile(chars[i],'g')
	    newString = newString.replace (chars[i], '&' + entities[i] + ';');
	  }
	  return newString;
	}
WebFontConfig = {
  google: {
	  families: ['Raleway:400,700,700:latin', 'Merriweather:400,700italic,700,400italic:latin']
	}
};
(function() {
	var wf = document.createElement('script');
	wf.src = ('//ajax.googleapis.com/ajax/libs/webfont/1.5.10/webfont.js');
	wf.type = 'text/javascript';
	wf.async = 'true';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(wf, s);
})(); 
$(".mfp-close").click(function(){
	$(".newsletter-popupregistrieren").magnificPopup('close');
});
$(document).ready(function() {
	
	$("div.attributes > table > tbody > tr:contains('Farbe')").find("td > table > tbody > tr > td").each(function(){
		if (farben[$(this).html().htmlEntities()]) {
			$(this).removeClass("attrpopup").addClass("farbbox masterTooltip").addClass(farben[$(this).html().htmlEntities()].replace("/","")).attr("title", $(this).html());
		} else {
			console.log($(this).html());
			console.log(farben);
			console.log($(this).html().htmlEntities());
			
		}
	});
    $(".footmUL>li>a").removeAttr("href");
    $(".ans_shop_media .additionalpics_container .additionalpics").parent().slick({
	  vertical: true,
	  slidesToShow: 4,
	  slidesToScroll: 2,
	  nextArrow: '<button type="button" class="slick-next"></button>',
	  prevArrow: '<button type="button" class="slick-prev"></button>'
	});
    $(".newsletter-popupregistrieren").click(function(e){
		$.magnificPopup.open({
		  items: {
				src: "iframe.white-popup",
			},
			showCloseBtn: true,
			type: 'inline',
			mainClass: 'iframe-newsletter',
		});
	});
	$("a[href*='/about-us/newsletter']").click(function(e){
		e.preventDefault();
		$(".newsletter-popupregistrieren").trigger("click");
	});
	$("#site_head ul.headmUL li.listing span.headline a:contains('Alles aus Shop by Look')").html("Alle Outfits");
	$("#site_head ul.headmUL li.listing span.headline a:contains('Alles aus Designer')").html("Designer von A-Z");

	$(".slider-style").slick({
		slidesToShow: 3,
		variableWidth: true,
		nextArrow: '<button type="button" class="slick-next"></button>',
		prevArrow: '<button type="button" class="slick-prev"></button>'
	});
	$('.open-popup-link').magnificPopup({      type:'inline',      midClick: true    });    
	$('.ajax-popup').magnificPopup({
		  type: 'ajax',
		  callbacks: {
			  parseAjax: function(mfpResponse) {
				  // mfpResponse.data is a "data" object from ajax "success" callback
				  // for simple HTML file, it will be just String
				  // You may modify it to change contents of the popup
				  // For example, to show just #some-element:
				  mfpResponse.data = $(mfpResponse.data).find('#site_article_content');

				  // mfpResponse.data must be a String or a DOM (jQuery) element

				  console.log('Ajax content loaded:', mfpResponse);
			  },
			  ajaxContentAdded: function() {
				  // Ajax content is loaded and appended to DOM
				  console.log(this.content);
			}
		}
	});
	$('.ajax-popup-versand').magnificPopup({
		  type: 'ajax',
		  mainClass: 'ajax-versand',
		  callbacks: {
			  parseAjax: function(mfpResponse) {
				  // mfpResponse.data is a "data" object from ajax "success" callback
				  // for simple HTML file, it will be just String
				  // You may modify it to change contents of the popup
				  // For example, to show just #some-element:
				  mfpResponse.data = $(mfpResponse.data).find('#site_article_content');

				  // mfpResponse.data must be a String or a DOM (jQuery) element

				  console.log('Ajax content loaded:', mfpResponse);
			  },
			  ajaxContentAdded: function() {
				  // Ajax content is loaded and appended to DOM
				  console.log(this.content);
			  }
		  }
	  });
	$('.ajax-popup-size').magnificPopup({
		  type: 'ajax',
		  mainClass: 'ajax-size',
		  callbacks: {
			  parseAjax: function(mfpResponse) {
				  // mfpResponse.data is a "data" object from ajax "success" callback
				  // for simple HTML file, it will be just String
				  // You may modify it to change contents of the popup
				  // For example, to show just #some-element:
				  mfpResponse.data = $(mfpResponse.data).find('#site_article_content');

				  // mfpResponse.data must be a String or a DOM (jQuery) element

				  console.log('Ajax content loaded:', mfpResponse);
			  },
			  ajaxContentAdded: function() {
				  // Ajax content is loaded and appended to DOM
				  console.log(this.content);
			  }
		  }
	  });
	$('.ajax-popup-link').magnificPopup({        type: 'iframe',    });
	$('.ajax-popup-link-register').magnificPopup({        type: 'iframe', mainClass: 'iframe-register',   });
	//Modern Attribute Picker
	$(".modern-selectbox").find(".titel-modern").hide();

	
	
	$(".ans_shop_attr_groessen .attrmatrix select").each(function(index){
        updateAttributeList(this,index,0);
  	});
	$(".modern-selectbox div span").click(function() {
		clickfunction(this,0);
	});
});

function clickfunction(those,repeat) {
	$(those).parent().parent().parent().parent().find("select[name='"+$(those).parent().attr("data-attr")+"'] option:eq("+$(those).attr("data-attr")+")").prop('selected',true);
	$(those).parent().parent().parent().parent().find("select[name='"+$(those).parent().attr("data-attr")+"']").change();
	$(those).parent().children('span').not(those).removeClass('active');
	$(those).addClass('active');
	if ($(those).parent().attr("data-attr").indexOf("Farbe")) {
		$(those).parent().parent().parent().parent().find("select[name='Farben'] option:eq(1)").prop('selected',true);
		$(those).parent().parent().parent().parent().find("select[name='Farben']").change();
	}
	$(those).parent().parent().parent().parent().parent().parent().find(".ans_shop_detail_basket.basketshop a.shop").addClass("wait-grey");
	var basketaction = $(those).parent().parent().parent().parent().parent().parent().find(".ans_shop_detail_basket.basketshop a.shop").attr("onclick");
	$(those).parent().parent().parent().parent().parent().parent().find(".ans_shop_detail_basket.basketshop a.shop").prop("onclick", "");
	var box= those;
	setTimeout(function(){
		if ($(box).parent().attr("data-attr").indexOf("Farbe")<0) {
			$(".ans_shop_attr_groessen .attrmatrix select").each(function(index){
				updateAttributeList(this,index,1);
			});
		}
		$(box).parent().parent().parent().parent().find('.soldout').removeClass("soldout");
		$(box).parent().parent().parent().parent().find("option.shop_disabled").each(function(){
			$(box).parent().parent().parent().parent().parent().parent().parent().parent().parent().find(".modern-selectbox div[data-attr='"+$(this).parent().attr("name")+"']").children().eq( $(this).index()-1 ).addClass("soldout");
		});

		$(box).parent().parent().parent().parent().parent().parent().find(".ans_shop_detail_basket.basketshop a.shop").attr("onclick", basketaction);
		$(".additionalpics_container .additionalpics img").each(function(){
			$(this).parent().attr("href",$(this).attr("src"));
			$(this).parent().attr("data-href",$(this).attr("src"));
		});    
		$(".modern-selectbox div:not('.groesse-attr') > div.data-attributes span").click(function() {
			clickfunction(this,1);
		});
		$(box).parent().parent().parent().parent().parent().parent().find(".ans_shop_detail_basket.basketshop a.shop").removeClass("wait-grey");
		var hasHover = $._data($('.masterTooltip')[0],'events')['mouseover'] && $._data($('.masterTooltip')[0],'events')['mouseout'];
		if (!hasHover) {
			$('.masterTooltip').hover(function(){
				// Hover over code
				var title = $(this).attr('title');
				$(this).data('tipText', title).removeAttr('title');
				$('<p class="tooltip"></p>')
				.text(title)
				.appendTo('body')
				.fadeIn('slow');
			}, function() {
				// Hover out code
				$(this).attr('title', $(this).data('tipText'));
				$('.tooltip').remove();
			}).mousemove(function(e) {
				var mousex = e.pageX + 0; //Get X coordinates
				var mousey = e.pageY + 10; //Get Y coordinates
				$('.tooltip')
				.css({ top: mousey, left: mousex })
			});
		};
		
	}, 1000);
}
function updateAttributeList (those,index,update) {
	  if ((trim($(those).attr("data-attr")).indexOf("Gr")>-1)&& (update<1)) {
		  var container = $(those).parent().parent().parent().parent().parent().parent().parent().parent().find(".modern-selectbox .groesse-attr .data-attributes");
		  container.html("");
		  $(container).attr("data-attr",$(those).attr("name"));
		  $(those).children().each(function(index){	
			  if (index>0) {
				var text = $(this).text().replace("Campina ","").replace("Campus ","").replace("iPhone ","").replace("iPhone ","");
				  if (text.indexOf("ml.")>=0) {
					  $(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().find(".modern-selectbox .gr a").hide();
				  }
				  if (text.indexOf("XXXL")>=0) {
					if($(this).attr("class")) {
						if($(this).attr("class").indexOf("shop_disabled")>=0) {
							$(container).append("<span data-attr='"+index+"' class='soldout xxl'>XX<br>XL</span>");
						}
					} else {
						$(container).append("<span data-attr='"+index+"' class='xxl'>XX<br>XL</span>");
					}
				  }else {
					if($(this).attr("class")) {
						if($(this).attr("class").indexOf("shop_disabled")>=0) {
							$(container).append("<span data-attr='"+index+"' class='soldout'>"+text.replace(" zur Warteliste","")+"</span>");
						}
					} else {
						$(container).append("<span data-attr='"+index+"'>"+text+"</span>");
					} 
				  }
		  }
		});
		$(those).parent().parent().parent().parent().parent().parent().parent().parent().find(".modern-selectbox .gr").show();
	} 
	if ((trim($(those).attr("data-attr")).indexOf("Wert")>-1)&& (update<1)) {
		var container = $(those).parent().parent().parent().parent().parent().parent().parent().parent().find(".modern-selectbox .wert-attr .data-attributes");
		  container.html("");
		$(container).attr("data-attr",$(those).attr("name"));
		$(those).children().each(function(index){
			if (index>0) {
				if($(this).attr("class")) {
					if($(this).attr("class").indexOf("shop_disabled")>=0) {
						$(container).append("<span data-attr='"+index+"' class='soldout'>"+$(this).text().replace(" zur Warteliste","")+"</span>");
					}
				} else {
						$(container).append("<span data-attr='"+index+"'>"+$(this).text()+"</span>");
				} 
			}
		});
		$(those).parent().parent().parent().parent().parent().parent().parent().parent().find(".modern-selectbox .wert").show();
	} 
		if ((trim($(those).attr("data-attr")).indexOf("Brenn")>-1)&& (update<1)) {
			var container = $(those).parent().parent().parent().parent().parent().parent().parent().parent().find(".modern-selectbox .brenn-attr .data-attributes");
		  container.html("");
			$(container).attr("data-attr",$(those).attr("name"));
			$(those).children().each(function(index){
			  if (index>0) {
				if($(this).attr("class")) {
					if($(this).attr("class").indexOf("shop_disabled")>=0) {
						$(container).append("<span data-attr='"+index+"' class='soldout'>"+$(this).text().replace(" zur Warteliste","")+"</span>");
					}
				} else {
					$(container).append("<span data-attr='"+index+"'>"+$(this).text()+"</span>");
				} 
			}
		});
		$(those).parent().parent().parent().parent().parent().parent().parent().parent().find(".modern-selectbox .brenn").show();
	} 

	if (trim($(those).attr("data-attr")).indexOf("Farbe")>-1)  {
		 var container = $(those).parent().parent().parent().parent().parent().parent().parent().parent().find(".modern-selectbox .farbe-attr .data-attributes");
		  container.html("");
			  $(container).attr("data-attr",$(those).attr("name"));
			  $(those).children().each(function(index){
				if (index>0) {
					if($(this).attr("class")) {
						if($(this).attr("class").indexOf("shop_disabled")>=0) {
							$(container).append("<span title='"+$(this).text().replace(" zur Warteliste","")+"' data-attr='"+index+"' class='masterTooltip soldout "+farben[$(this).text().htmlEntities().replace(" zur Warteliste","")].replace("/","")+"'></span>");
						}
					} else {
						$(container).append("<span title='"+$(this).text()+"' data-attr='"+index+"' class='masterTooltip "+farben[$(this).text().htmlEntities().replace(" zur Warteliste","")].replace("/","")+"'></span>");
					}
				}
			});
		$(those).parent().parent().parent().parent().parent().parent().parent().parent().find(".modern-selectbox .far").show();
	}
}

    runOnLoad(function() {

  $('.masterTooltip').hover(function(){
        // Hover over code
        var title = $(this).attr('title');
        $(this).data('tipText', title).removeAttr('title');
        $('<p class="tooltip"></p>')
        .text(title)
        .appendTo('body')
        .fadeIn('slow');
  }, function() {
        // Hover out code
        $(this).attr('title', $(this).data('tipText'));
        $('.tooltip').remove();
  }).mousemove(function(e) {
        var mousex = e.pageX + 0; //Get X coordinates
        var mousey = e.pageY + 10; //Get Y coordinates
        $('.tooltip')
        .css({ top: mousey, left: mousex })
  });
    });

