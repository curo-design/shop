// JavaScript Document

	$(function(){

	
	function animationClick(trigger, element, animation){
    element = $(element);
	trigger = $(trigger);
    trigger.click(
        function() {
            element.addClass('animated ' + animation);          
            //wait for animation to finish before removing classes
            window.setTimeout( function(){
                element.removeClass('animated ' + animation);
            }, 2000);           
 
        });
}

	function animationHover(trigger, element, animation){
    element = $(element);
    trigger = $(trigger);
    trigger.hover(
        function() {
            element.addClass('animated ' + animation);          
        },
        function(){
            //wait for animation to finish before removing classes
            window.setTimeout( function(){
                element.removeClass('animated ' + animation);
            }, 2000);           
        });
}
	animationHover ('hover', '#site_head', 'pulse');

});
// Google Analytics Code, nicht l�schen!
(function(i,s,o,g,r,a,m) {
    i['GoogleAnalyticsObject']  = r;
    i[r]    = i[r]||function() {
        (i[r].q=i[r].q||[]).push(arguments)
    },
    i[r].l  = 1*new Date();
    a       = s.createElement(o),
    m       = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src   = g;
    m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-64173979-1', 'auto');
ga('send', 'pageview');